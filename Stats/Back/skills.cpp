#include "skills.hpp"

Abilities::Abilities(QString filename) :
  Serializable{filename}
{
  deserialize();
}

Abilities::~Abilities()
{
  serialize();
}

void Abilities::changeSkill(Skills skill, qint32 value)
{
    mSkills[skill] = value;
}

void Abilities::change(const SkillMap &abilities)
{
  auto iter = mSkills.begin();
  auto end = mSkills.end();
  auto abilities_iter = abilities.begin();
  for (; iter != end; ++iter, ++abilities_iter)
    {
      iter->second += abilities_iter->second;
    }
}

qint32 Abilities::getSkill(Skills skill) const
{
  return mSkills.at(skill);
}

SkillMap Abilities::getSkills() const
{
  return mSkills;
}

void Abilities::serializeType()
{
  *mIO << mSkills.at(Skills::ACROBATICS)
       << mSkills.at(Skills::SNEAK)
       << mSkills.at(Skills::GUNS)
       << mSkills.at(Skills::NATURE)
       << mSkills.at(Skills::ENERGY_WEAPONS)
       << mSkills.at(Skills::HISTORY)
       << mSkills.at(Skills::RELIGION)
       << mSkills.at(Skills::SCIENCE)
       << mSkills.at(Skills::REPAIR)
       << mSkills.at(Skills::MEDICINE)
       << mSkills.at(Skills::PERFORMANCE)
       << mSkills.at(Skills::PERSUASION)
       << mSkills.at(Skills::DECEPTION)
       << mSkills.at(Skills::BARTER)
       << mSkills.at(Skills::RESISTANCE)
       << mSkills.at(Skills::SURVIVAL)
       << mSkills.at(Skills::DETECTION)
       << mSkills.at(Skills::ANIMAL_CARING)
       << mSkills.at(Skills::LOCKPICK)
       << mSkills.at(Skills::EXPLOSIVES)
       << mSkills.at(Skills::UNARMED)
       << mSkills.at(Skills::ATHLETICS)
       << mSkills.at(Skills::MELEE_WEAPONS);
}

void Abilities::deserializeType()
{
  *mIO >> mSkills.at(Skills::ACROBATICS)
       >> mSkills.at(Skills::SNEAK)
       >> mSkills.at(Skills::GUNS)
       >> mSkills.at(Skills::NATURE)
       >> mSkills.at(Skills::ENERGY_WEAPONS)
       >> mSkills.at(Skills::HISTORY)
       >> mSkills.at(Skills::RELIGION)
       >> mSkills.at(Skills::SCIENCE)
       >> mSkills.at(Skills::REPAIR)
       >> mSkills.at(Skills::MEDICINE)
       >> mSkills.at(Skills::PERFORMANCE)
       >> mSkills.at(Skills::PERSUASION)
       >> mSkills.at(Skills::DECEPTION)
       >> mSkills.at(Skills::BARTER)
       >> mSkills.at(Skills::RESISTANCE)
       >> mSkills.at(Skills::SURVIVAL)
       >> mSkills.at(Skills::DETECTION)
       >> mSkills.at(Skills::ANIMAL_CARING)
       >> mSkills.at(Skills::LOCKPICK)
       >> mSkills.at(Skills::EXPLOSIVES)
       >> mSkills.at(Skills::UNARMED)
       >> mSkills.at(Skills::ATHLETICS)
       >> mSkills.at(Skills::MELEE_WEAPONS);

}

Abilities::Abilities(const SkillMap& abilities, QString filename) : Serializable(filename)
{
  mSkills = abilities;
}

Abilities& Abilities::operator=(const SkillMap& abilities)
{
  mSkills = abilities;
  return *this;
}

qint32 Abilities::at(Skills skill) const
{
  return mSkills.at(skill);
}
