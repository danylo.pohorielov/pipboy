#ifndef SKILL_HPP
#define SKILL_HPP

#include "entity.hpp"

enum class Skills
{
  MELEE_WEAPONS = 0,
  ATHLETICS = 1,
  UNARMED = 2,
  EXPLOSIVES = 3,
  LOCKPICK = 4,
  ANIMAL_CARING = 5,
  DETECTION = 6,
  SURVIVAL = 7,
  RESISTANCE = 8,
  BARTER = 9,
  DECEPTION = 10,
  PERSUASION = 11,
  PERFORMANCE = 12,
  MEDICINE = 13,
  REPAIR = 14,
  SCIENCE = 15,
  RELIGION = 16,
  HISTORY = 17,
  ENERGY_WEAPONS = 18,
  NATURE = 19,
  GUNS = 20,
  SNEAK = 21,
  ACROBATICS = 22
};

static const std::map<const Skills, const QString> gSkillNames =
{
  {Skills::MELEE_WEAPONS,   "MELEE WEAPONS"},
  {Skills::ATHLETICS,       "ATHLETICS"},
  {Skills::UNARMED,         "UNARMED"},
  {Skills::EXPLOSIVES,      "EXPLOSIVES"},
  {Skills::LOCKPICK,        "LOCKPICK"},
  {Skills::ANIMAL_CARING,   "ANIMAL CARING"},
  {Skills::DETECTION,       "DETECTION"},
  {Skills::SURVIVAL,        "SURVIVAL"},
  {Skills::RESISTANCE,      "RESISTANCE"},
  {Skills::BARTER,          "BARTER"},
  {Skills::DECEPTION,       "DECEPTION"},
  {Skills::PERSUASION,      "PERSUASION"},
  {Skills::PERFORMANCE,     "PERFORMANCE"},
  {Skills::MEDICINE,        "MEDICINE"},
  {Skills::REPAIR,          "REPAIR"},
  {Skills::SCIENCE,         "SCIENCE"},
  {Skills::RELIGION,        "RELIGION"},
  {Skills::HISTORY,         "HISTORY"},
  {Skills::ENERGY_WEAPONS,  "ENERGY WEAPONS"},
  {Skills::NATURE,          "NATURE"},
  {Skills::GUNS,            "GUNS"},
  {Skills::SNEAK,           "SNEAK"},
  {Skills::ACROBATICS,      "ACROBATICS"}
};

class Skill : public Entity
{
public:
  Skill(const QStringList& list);

  Skills skill();

  qint32 value();

  void setValue(qint32 value);

  void changeValue(qint32 value);

private:
  Skills mSkill;
  qint32 mValue;
};

class SkillReader : public EntityReader
{
public:
  SkillReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // SKILL_HPP
