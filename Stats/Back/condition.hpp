#ifndef CONDITION_HPP
#define CONDITION_HPP

#include <QObject>
#include <QLabel>
//#include "serializable.hpp"

class Condition : public QObject// : public Serializable
{
  Q_OBJECT
public:
  Condition();

  void setUp();

  qint32 currentHunger() const;
  qint32 maxHunger() const;
  void setMaxHunger(qint32);

  qint32 currentThirst() const;
  qint32 maxThirst() const;
  void setMaxThirst(qint32);

  qint32 currentSleepExhaustion() const;
  qint32 maxSleepExhaustion() const;
  void setMaxSleepExhaustion(qint32);

  qint32 currentRadiation() const;
  qint32 maxRadiation() const;
  void setMaxRadiation(qint32);

  void setCurrentHunger(qint32);
  void setCurrentThirst(qint32);
  void setCurrentSleepExhaustion(qint32);
  void setCurrentRadiation(qint32);

signals:
  void hungerChanged(qint32 currentHunger, qint32 maxHunger);
  void thirstChanged(qint32 currentThirst, qint32 maxThirst);
  void sleepExhaustionChanged(qint32 currentSleepExhaustion, qint32 maxSleepExhaustion);
  void radiationChanged(qint32 currentRadiation, qint32 maxRadiation);

private:

  qint32 mCurrentHunger = 0;
  qint32 mMaxHunger = 1000;

  qint32 mCurrentThirst = 0;
  qint32 mMaxThirst = 1000;

  qint32 mCurrentSleepExhaustion = 0;
  qint32 mMaxSleepExhaustion = 1000;

  qint32 mCurrentRadiation = 0;
  qint32 mMaxRadiation = 1000;

  // Serializable interface
//public:
//  void serializeType();
//  void deserializeType();
};

#endif // CONDITION_HPP
