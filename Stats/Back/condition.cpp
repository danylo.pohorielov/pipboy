#include "Stats/Back/condition.hpp"

Condition::Condition()
  //: Serializable(":/txts/resources/data/character.txt")
{
  //deserialize();
}

void Condition::setUp()
{
  emit hungerChanged(mCurrentHunger, mMaxHunger);
  emit thirstChanged(mCurrentThirst, mMaxThirst);
  emit sleepExhaustionChanged(mCurrentSleepExhaustion, mMaxSleepExhaustion);
  emit radiationChanged(mCurrentRadiation, mMaxRadiation);
}

qint32 Condition::currentHunger() const
{
  return mCurrentHunger;
}

void Condition::setCurrentHunger(qint32 currentHunger)
{
  mCurrentHunger = currentHunger;
  emit hungerChanged(mCurrentHunger, mMaxHunger);
}

qint32 Condition::maxHunger() const
{
  return mMaxHunger;
}

void Condition::setMaxHunger(qint32 maxHunger)
{
  mMaxHunger = maxHunger;
  emit hungerChanged(mCurrentHunger, mMaxHunger);
}

qint32 Condition::currentThirst() const
{
  return mCurrentThirst;
}

void Condition::setCurrentThirst(qint32 currentThirst)
{
  mCurrentThirst = currentThirst;
  emit thirstChanged(mCurrentThirst, mMaxThirst);
}

qint32 Condition::maxThirst() const
{
  return mMaxThirst;
}

void Condition::setMaxThirst(qint32 maxThirst)
{
  mMaxThirst = maxThirst;
  emit thirstChanged(mCurrentThirst, mMaxThirst);
}

qint32 Condition::currentSleepExhaustion() const
{
  return mCurrentSleepExhaustion;
}

void Condition::setCurrentSleepExhaustion(qint32 currentSleepExhaustion)
{
  mCurrentSleepExhaustion = currentSleepExhaustion;
  emit sleepExhaustionChanged(mCurrentSleepExhaustion, mMaxSleepExhaustion);
}

qint32 Condition::maxSleepExhaustion() const
{
  return mMaxSleepExhaustion;
}

void Condition::setMaxSleepExhaustion(qint32 maxSleepExhaustion)
{
  mMaxSleepExhaustion = maxSleepExhaustion;
  emit sleepExhaustionChanged(mCurrentSleepExhaustion, mMaxSleepExhaustion);
}

qint32 Condition::currentRadiation() const
{
  return mCurrentRadiation;
}

void Condition::setCurrentRadiation(qint32 currentRadiation)
{
  mCurrentRadiation = currentRadiation;
  emit radiationChanged(mCurrentRadiation, mMaxRadiation);
}

qint32 Condition::maxRadiation() const
{
  return mMaxRadiation;
}

void Condition::setMaxRadiation(qint32 maxRadiation)
{
  mMaxRadiation = maxRadiation;
  emit radiationChanged(mCurrentRadiation, mMaxRadiation);
}

//void Condition::serializeType()
//{
////  *mIO << mCurrentHunger
////       << mCurrentThirst
////       << mCurrentSleepExhaustion
////       << mCurrentRadiation
////       << mCurrentLevel
////       << mCurrentExpirience;
//}

//void Condition::deserializeType()
//{
////  *mIO >> mCurrentHunger
////       >> mCurrentThirst
////       >> mCurrentSleepExhaustion
////       >> mCurrentRadiation
////       >> mCurrentLevel
////       >> mCurrentExpirience;
//}
