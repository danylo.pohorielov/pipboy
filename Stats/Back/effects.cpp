#include "Stats/Back/effects.hpp"

#include <QString>

#include <sstream>

//void Effects::revert(StatMap &characterStats, SkillMap &characterSkills) const
//{
//  for (auto& effect : mEffects)
//    {
//      effect.revert(characterStats, characterSkills);
//    }
//}

//void Effects::apply(StatMap &characterStats, SkillMap &characterSkills) const
//{
//  for (auto& effect : mEffects)
//    {
//      effect.apply(characterStats, characterSkills);
//    }
//}

void Effect::apply(StatMap &characterStats, SkillMap &characterSkills) const
{
  for (const auto& mod : mStatMods) {
      characterStats[mod.first] += mod.second;
  }
  for (const auto& mod : mSkillMods) {
      characterSkills[mod.first] += mod.second;
  }
}

void Effect::revert(StatMap &characterStats, SkillMap &characterSkills) const
{
  for (const auto& mod : mStatMods) {
      characterStats[mod.first] -= mod.second;
  }
  for (const auto& mod : mSkillMods) {
      characterSkills[mod.first] -= mod.second;
    }
}

Effect::Effect(const QString &name, int duration,
               const StatMap &statMods,
               const SkillMap &skillMods)
  : Entity(name), mDuration(duration), mStatMods(statMods), mSkillMods(skillMods)
{

}

Effect::Effect(const QStringList &data)
{
  qint32 index = 0;
  mName = data[index++];
  mDuration = data[index++].toInt();
  initStats(data[index++].split(';'));
  initSkills(data[index++].split(';'));
}

void Effect::initStats(const QStringList &list)
{
  QStringList temp;
  QStringList values;
  for (int index = 0; index < list.size(); ++index) {
      temp = list[index].split(';');
      for (int indey = 0; indey < temp.size(); ++indey) {
          values = temp[indey].split(' ');
          mStatMods[static_cast<Stats>(values[0].toInt())] = values[1].toInt();
        }
    }
}

void Effect::initSkills(const QStringList &list)
{
  QStringList temp;
  QStringList values;
  for (int index = 0; index < list.size(); ++index) {
      temp = list[index].split(';');
      for (int indey = 0; indey < temp.size(); ++indey) {
          values = temp[indey].split(' ');
          mSkillMods[static_cast<Skills>(values[0].toInt())] = values[1].toInt();
        }
    }
}

QString Effect::mapStats()
{
  if(mStatMods.size() == 0)
    return "";
  QString data;
  for(auto iter : mStatMods) {
      data += gStatNames.at(iter.first) + ": "
          + (iter.second > 0 ? "+" : "") + QString::number(iter.second) + "<br>";
    }
  return data;
}

QString Effect::mapSkills()
{
  if(mSkillMods.size() == 0)
    return "";
  QString data;
  for(auto iter : mSkillMods) {
      data += gSkillNames.at(iter.first) + ": "
          + (iter.second > 0 ? "+" : "") + QString::number(iter.second) + "<br>";
    }
  return data;
}

QString Effect::toString()
{
  return "Name: " + mName + " [ ";
}
