#ifndef STATS_H
#define STATS_H

#include <QWidget>
#include <QDataStream>

#include "serializable.hpp"
#include "stat.hpp"

using StatMap = std::map<Stats, qint32>;

class Characteristics : public Serializable
{
public:
  Characteristics(QString filename = ":/txts/resources/data/stats.txt");

  Characteristics(const StatMap& stats, QString filename);

  Characteristics& operator=(const StatMap& stats);

  qint32 at(Stats stat);

  ~Characteristics();

  void changeStat(Stats stat, qint32 value);

  void change(const StatMap& abilities);

  qint32 getStat(Stats stat) const;

  StatMap getStats();

private:
  StatMap mSpecial =
  {
    {Stats::STRENGTH, 10},
    {Stats::PERCEPTION, 10},
    {Stats::ENDURANCE, 10},
    {Stats::CHARISMA, 10},
    {Stats::INTELLIGENCE, 10},
    {Stats::AGILITY, 10},
    {Stats::LUCK, 10}
  };

  QStringList mStatsNames;

  // Serializable interface
public:
  void serializeType();
  void deserializeType();
};



#endif // STATS_H
