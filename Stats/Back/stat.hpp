#ifndef STAT_HPP
#define STAT_HPP

#include <QTCore>
#include "entity.hpp"

enum class Stats
{
  STRENGTH      = 0,
  PERCEPTION    = 1,
  ENDURANCE     = 2,
  CHARISMA      = 3,
  INTELLIGENCE  = 4,
  AGILITY       = 5,
  LUCK          = 6
};

static const std::map<Stats, QString> gStatNames =
{
  {Stats::STRENGTH,     "STRENGTH"},
  {Stats::PERCEPTION,   "PERCEPTION"},
  {Stats::ENDURANCE,    "ENDURANCE"},
  {Stats::CHARISMA,     "CHARISMA"},
  {Stats::INTELLIGENCE, "INTELLIGENCE"},
  {Stats::AGILITY,      "AGILITY"},
  {Stats::LUCK,         "LUCK"}
};

class Stat : public Entity
{
public:
  Stat(const QStringList& list);

  Stats stat();

  qint32 value();

  void setValue(qint32 value);

  void changeValue(qint32 value);

private:
  Stats mStat;
  qint32 mValue;
};

class StatReader : public EntityReader
{
public:
  StatReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // STAT_HPP
