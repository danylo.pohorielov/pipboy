#ifndef SERIALIZABLE_HPP
#define SERIALIZABLE_HPP

#include <QFile>
#include <QTextStream>
#include <memory>

class Serializable
{
public:
  Serializable(QString filename);

  ~Serializable();

  void serialize();

  virtual void serializeType() = 0;

  void deserialize();

  virtual void deserializeType() = 0;

protected:
  QString mFilename;
  QFile mFile;
  std::unique_ptr<QTextStream> mIO;
};

#endif // SERIALIZABLE_HPP
