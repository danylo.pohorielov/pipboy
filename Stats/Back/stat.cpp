#include "stat.hpp"

Stat::Stat(const QStringList &list)
{
  qint32 index = 0;
  mName = list[index++];
  mStat = static_cast<Stats>(list[index++].toInt());
  mValue = list[index++].toInt();
}

Stats Stat::stat()
{
  return mStat;
}

qint32 Stat::value()
{
  return mValue;
}

void Stat::setValue(qint32 value)
{
  mValue = value;
}

void Stat::changeValue(qint32 value)
{
  mValue += value;
}

StatReader::StatReader(const QString &filename) : EntityReader{filename}
{

}

std::unique_ptr<Entity> StatReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Stat>(line.split(';'));
}
