#ifndef PERKS_HPP
#define PERKS_HPP

#include <QString>
#include <QImage>

#include "effects.hpp"

class Perk
{
public:
  Perk(QString perk = "");

  bool parsePerk(QString perk);

private:

  std::unique_ptr<Effect> mEffect;
  QString mDescription;
  QImage mImage;
};

class Perks
{
public:
  Perks();

  bool readPerks(QString filename);

  bool savePerks(QString filename);

  ~Perks();

private:

  std::vector<Perk> mPerks;
};

#endif // PERKS_HPP
