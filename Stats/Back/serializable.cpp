#include "serializable.hpp"
#include <QDebug>

Serializable::Serializable(QString filename)
  : mFilename{filename}, mFile{filename}
{

}

Serializable::~Serializable()
{
}

void Serializable::serialize()
{
  if (!mFile.open(QIODevice::WriteOnly)) {
        qDebug() << "here";
        throw "Couldn't open file.";
      }
  mIO = std::make_unique<QTextStream>(&mFile);
  serializeType();
  mIO.reset();
  mFile.close();
}

void Serializable::deserialize()
{
  if (!mFile.open(QIODevice::ReadOnly | QIODevice::Truncate)) {
        qDebug() << "there";
        throw "Couldn't open file.";
      }
  mIO = std::make_unique<QTextStream>(&mFile);
  deserializeType();
  mIO.reset();
  mFile.close();
}

