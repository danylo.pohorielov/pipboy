#ifndef SKILLS_HPP
#define SKILLS_HPP

#include <QWidget>
#include <QDataStream>

#include "serializable.hpp"
#include "skill.hpp"

using SkillMap = std::map<Skills, qint32>;

class Abilities : public Serializable
{
public:
  Abilities(QString filename = ":/txts/resources/data/stats.txt");

  Abilities(const SkillMap& abilities, QString filename);

  Abilities& operator=(const SkillMap& abilities);

  qint32 at(Skills skill) const;

  ~Abilities();

  void changeSkill(Skills skill, qint32 value);

  void change(const SkillMap& abilities);

  qint32 getSkill(Skills skill) const;

  SkillMap getSkills() const;

private:
  SkillMap mSkills =
  {
    {Skills::MELEE_WEAPONS,   0},
    {Skills::ATHLETICS,       0},
    {Skills::UNARMED,         0},
    {Skills::EXPLOSIVES,      0},
    {Skills::LOCKPICK,        0},
    {Skills::ANIMAL_CARING,   0},
    {Skills::DETECTION,       0},
    {Skills::SURVIVAL,        0},
    {Skills::RESISTANCE,      0},
    {Skills::BARTER,          0},
    {Skills::DECEPTION,       0},
    {Skills::PERSUASION,      0},
    {Skills::PERFORMANCE,     0},
    {Skills::MEDICINE,        0},
    {Skills::REPAIR,          0},
    {Skills::SCIENCE,         0},
    {Skills::RELIGION,        0},
    {Skills::HISTORY,         0},
    {Skills::ENERGY_WEAPONS,  0},
    {Skills::NATURE,          0},
    {Skills::GUNS,            0},
    {Skills::SNEAK,           0},
    {Skills::ACROBATICS,      0}
  };

  QStringList mSkillsNames;

  // Serializable interface
public:
  void serializeType();
  void deserializeType();
};

#endif // SKILLS_HPP
