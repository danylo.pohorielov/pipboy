#include "stats.hpp"

Characteristics::Characteristics(QString filename) :
  Serializable{filename}
{
  //deserialize();
  //mSpecialNames.
}

Characteristics::Characteristics(const StatMap &stats, QString filename)
  : Serializable{filename}
{
  mSpecial = stats;
}

Characteristics &Characteristics::operator=(const StatMap &stats)
{
  mSpecial = stats;
  return *this;
}

qint32 Characteristics::at(Stats stat)
{
  return mSpecial.at(stat);
}

Characteristics::~Characteristics()
{
  //serialize();
}

void Characteristics::changeStat(Stats stat, qint32 value)
{
  mSpecial.at(stat) = value;
  //mSpecialNames.indexOf()
}

void Characteristics::change(const StatMap &stats)
{
  auto iter = mSpecial.begin();
  auto end = mSpecial.end();
  auto abilities_iter = stats.begin();
  for (; iter != end; ++iter, ++abilities_iter)
    {
      iter->second += abilities_iter->second;
    }
}

qint32 Characteristics::getStat(Stats stat) const
{
  return mSpecial.at(stat);
}

StatMap Characteristics::getStats()
{
  return mSpecial;
}

void Characteristics::serializeType()
{
  *mIO << mSpecial.at(Stats::STRENGTH)
       << mSpecial.at(Stats::PERCEPTION)
       << mSpecial.at(Stats::ENDURANCE)
       << mSpecial.at(Stats::CHARISMA)
       << mSpecial.at(Stats::INTELLIGENCE)
       << mSpecial.at(Stats::AGILITY)
       << mSpecial.at(Stats::LUCK);
}

void Characteristics::deserializeType()
{
  *mIO >> mSpecial.at(Stats::STRENGTH)
       >> mSpecial.at(Stats::PERCEPTION)
       >> mSpecial.at(Stats::ENDURANCE)
       >> mSpecial.at(Stats::CHARISMA)
       >> mSpecial.at(Stats::INTELLIGENCE)
       >> mSpecial.at(Stats::AGILITY)
       >> mSpecial.at(Stats::LUCK);
}
