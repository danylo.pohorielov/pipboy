#ifndef EFFECTS_HPP
#define EFFECTS_HPP

#include <QObject>
#include <QWidget>
#include <QVector>
#include <QString>
#include <map>
#include <QStringList>
#include <QStringListModel>

#include "stats.hpp"
#include "skills.hpp"
//#include "entity.hpp"

static const std::map<const Stats, const std::vector<Skills>> gStatsAndTheirSkills =
{
  {Stats::STRENGTH,     {Skills::MELEE_WEAPONS, Skills::ATHLETICS,  Skills::UNARMED }},

  {Stats::PERCEPTION,   {Skills::EXPLOSIVES,    Skills::LOCKPICK,   Skills::ANIMAL_CARING,  Skills::DETECTION }},

  {Stats::ENDURANCE,    {Skills::UNARMED,       Skills::SURVIVAL,   Skills::RESISTANCE }},

  {Stats::CHARISMA,     {Skills::BARTER,        Skills::DECEPTION,  Skills::PERSUASION,     Skills::PERFORMANCE }},

  {Stats::INTELLIGENCE, {Skills::MELEE_WEAPONS, Skills::ATHLETICS,  Skills::UNARMED }},

  {Stats::AGILITY,      {Skills::MEDICINE,      Skills::SCIENCE,    Skills::REPAIR,
                         Skills::RELIGION,      Skills::HISTORY,    Skills::ENERGY_WEAPONS }},
  {Stats::LUCK, {}}
};

class Effect : public Entity
{
private:
  Effect();
public:
    Effect(const QString& name, int duration,
           const StatMap& statMods,
           const SkillMap& skillMods);

    Effect(const QStringList& data);

    int getDuration() const { return mDuration; }
    void setStatMods(StatMap& stats) {mStatMods = stats;};
    void setSkillMods(SkillMap& skills){mSkillMods = skills;};
    StatMap& getStatMods() { return mStatMods; }
    SkillMap& getSkillMods() { return mSkillMods; }
    void initStats(const QStringList &list);
    void initSkills(const QStringList &list);

    QString mapStats();

    QString mapSkills();

    QString toString();

    void apply(StatMap& characterStats, SkillMap& characterSkills) const ;

    void revert(StatMap& characterStats, SkillMap& characterSkills) const ;


private:
    int mDuration; // Duration in game turns or seconds
    StatMap mStatMods; // Map of stat modifications
    SkillMap mSkillMods; // Map of skill modifications
};

#endif // EFFECTS_HPP
