#include "skill.hpp"

Skill::Skill(const QStringList &list)
{
  qint32 index = 0;
  mName = list[index++];
  mSkill = static_cast<Skills>(list[index++].toInt());
  mValue = list[index++].toInt();
}

Skills Skill::skill()
{
  return mSkill;
}

qint32 Skill::value()
{
  return mValue;
}

void Skill::setValue(qint32 value)
{
  mValue = value;
}

void Skill::changeValue(qint32 value)
{
  mValue += value;
}

SkillReader::SkillReader(const QString &filename) : EntityReader{filename}
{

}

std::unique_ptr<Entity> SkillReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Skill>(line.split(';'));
}
