#include "Stats/Widgets/perkswidget.hpp"

PerksWidget::PerksWidget(QWidget *parent)
  : DescriptionWidget(parent)
{
  mDescription = new QLabel(this);
  mDescription->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mLayout->addWidget(mDescription, 1, 1);
}
