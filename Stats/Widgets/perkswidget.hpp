#ifndef PERKSWIDGET_HPP
#define PERKSWIDGET_HPP

#include "descriptionwidget.hpp"
#include <QWidget>

class PerksWidget : public DescriptionWidget
{
  Q_OBJECT
public:
  PerksWidget(QWidget *parent = nullptr);

  void fillInfo(Entity* entity)
  {

  }

private:
  QLabel* mDescription;
};

#endif // PERKSWIDGET_HPP
