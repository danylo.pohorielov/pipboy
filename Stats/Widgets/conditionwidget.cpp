#include "Stats/Widgets/conditionwidget.hpp"

#include <QLayout>

ConditionWidget::ConditionWidget(Condition* condition, QWidget *parent)
  : QWidget(parent), mCondition(condition)
{
  mLayout = new QGridLayout(this);
  setLayout(mLayout);

  shadowEffect = new QGraphicsDropShadowEffect();

  // Configure the shadow effect
  shadowEffect->setBlurRadius(20.0); // Adjust the blur radius to control the glow effect
  shadowEffect->setColor(Qt::green); // Set the color of the glow
  shadowEffect->setOffset(.0);

  mHunger       = new QLabel(this);
  mThirst       = new QLabel(this);
  mSleep        = new QLabel(this);
  mRadiation    = new QLabel(this);

  mHungerBar      = new QProgressBar(this);
  mThirstBar      = new QProgressBar(this);
  mSleepBar       = new QProgressBar(this);
  mRadiationBar   = new QProgressBar(this);

  mHungerBar->setRange(0, condition->maxHunger());
  mHungerBar->setTextVisible(true);
  mHungerBar->setFormat("%v/%m");

  mThirstBar->setRange(0, condition->maxThirst());
  mThirstBar->setTextVisible(true);
  mThirstBar->setFormat("%v/%m");

  mSleepBar->setRange(0, condition->maxSleepExhaustion());
  mSleepBar->setTextVisible(true);
  mSleepBar->setFormat("%v/%m");

  mRadiationBar->setRange(0, condition->maxRadiation());
  mRadiationBar->setTextVisible(true);
  mRadiationBar->setFormat("%v/%m");

//  applyShadowEffect(mHunger);
//  applyShadowEffect(mThirst);
//  applyShadowEffect(mSleep);
//  applyShadowEffect(mRadiation);

//  applyShadowEffect(mHungerBar);
//  applyShadowEffect(mThirstBar);
//  applyShadowEffect(mSleepBar);
//  applyShadowEffect(mRadiationBar);

  mHunger->     setText("Hunger");
  mThirst->     setText("Thirst");
  mSleep->      setText("Sleep");
  mRadiation->  setText("Rad");

  mLayout->addWidget(mHunger, 0, 0);
  mLayout->addWidget(mHungerBar, 0, 1);

  mLayout->addWidget(mThirst);
  mLayout->addWidget(mThirstBar);

  mLayout->addWidget(mSleep);
  mLayout->addWidget(mSleepBar);

  mLayout->addWidget(mRadiation);
  mLayout->addWidget(mRadiationBar);

  connect(condition, &Condition::hungerChanged, mHungerBar, &QProgressBar::setValue);
  connect(condition, &Condition::thirstChanged, mThirstBar, &QProgressBar::setValue);
  connect(condition, &Condition::sleepExhaustionChanged, mSleepBar, &QProgressBar::setValue);
  connect(condition, &Condition::radiationChanged, mRadiationBar, &QProgressBar::setValue);
}

void ConditionWidget::applyShadowEffect(QWidget *widget)
{
  auto shadowEffect = new QGraphicsDropShadowEffect();
  shadowEffect->setBlurRadius(20.0); // Adjust the blur radius to control the glow effect
  shadowEffect->setColor(Qt::green); // Set the color of the glow
  shadowEffect->setOffset(.0);
  widget->setGraphicsEffect(shadowEffect);
}
