#ifndef INFOWIDGET_HPP
#define INFOWIDGET_HPP

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include "character.hpp"
#include "time.hpp"

class InfoWidget : public QWidget
{
  Q_OBJECT
public:
  InfoWidget(Character* character, Time* time, QWidget *parent = nullptr);

  void fillInfo();

public slots:
  void setHealth(qint32 currentHealth, qint32 maxHealth);
  void setWeight(qint32 currentWeight, qint32 maxWeight);
  void setLevel(qint32 level);
  void setExpirience(qint32 expirience);
  void setTime(QString);

private:
  QHBoxLayout* mLayout;
  QLabel* mHealth;
  QLabel* mWeight;
  QLabel* mLevel;
  QLabel* mExpirience;
  QLabel* mTime;

  Character* mCharacter;
  Time* mCurrentTime;
};

#endif // INFOWIDGET_HPP
