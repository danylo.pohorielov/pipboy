#ifndef CHARACTERISTICSWIDGET_H
#define CHARACTERISTICSWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QListView>

#include "descriptionwidget.hpp"

#include "Stats/Back/effects.hpp"

class CharacteristicsWidget : public DescriptionWidget
{
  Q_OBJECT
public:
  CharacteristicsWidget(QWidget *parent = nullptr);

public slots:
  void fillInfo(Entity* entity) override;

private:
  QLabel* mDescription;
};

#endif // CHARACTERISTICSWIDGET_H
