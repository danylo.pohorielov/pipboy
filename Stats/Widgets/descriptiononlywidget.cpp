#include "Stats/Widgets/descriptiononlywidget.hpp"
#include "Stats/Back/effects.hpp"

DescriptionOnlyWidget::DescriptionOnlyWidget(QWidget *parent)
  : DescriptionWidget{parent}
{
  mDescription = new QLabel(this);
  mLayout->addWidget(mDescription);
}

void DescriptionOnlyWidget::fillInfo(Entity *entity)
{
  Effect* effect = static_cast<Effect*>(entity);
  QString temp = effect->mapStats() + effect->mapSkills();
  mDescription->setText(temp);
}
