#ifndef STATUSWIDGET_H
#define STATUSWIDGET_H

#include <QObject>
#include <QWidget>
#include <QTabWidget>

#include "conditionwidget.hpp"
#include "listpicturewidget.hpp"
#include "character.hpp"

class StatusWidget : public QTabWidget
{
  Q_OBJECT
public:
  StatusWidget(Character* character, QWidget *parent = nullptr);

private:
  ConditionWidget* mCondition;
  ListPictureWidget* mEffects;
};

#endif // STATUSWIDGET_H
