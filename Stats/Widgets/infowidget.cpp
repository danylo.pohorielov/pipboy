#include "infowidget.hpp"

InfoWidget::InfoWidget(Character* character, Time* time, QWidget *parent)
  : QWidget{parent}, mCharacter{character}, mCurrentTime{time}
{
  mLayout = new QHBoxLayout(this);

  mHealth       = new QLabel(this);
  mWeight       = new QLabel(this);
  mLevel        = new QLabel(this);
  mExpirience   = new QLabel(this);
  mTime         = new QLabel(this);

  mHealth->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

  mLayout->setMargin(0);
  mLayout->addWidget(mHealth);
  mLayout->addWidget(mWeight);
  mLayout->addWidget(mLevel);
  mLayout->addWidget(mExpirience);
  mLayout->addWidget(mTime);
  //fillInfo();
  connect(mCharacter, &Character::healthChanged, this, &InfoWidget::setHealth);
  connect(mCharacter, &Character::weightChanged, this, &InfoWidget::setWeight);
  connect(mCharacter, &Character::levelChanged, this, &InfoWidget::setLevel);
  connect(mCharacter, &Character::expirienceChanged, this, &InfoWidget::setExpirience);
  connect(mCurrentTime, &Time::timeChanged, this, &InfoWidget::setTime);
}

void InfoWidget::fillInfo()
{
//  mHealth->setText("<b>Health</b>: " + QString::number(mCharacter->currentHealth())
//                   + '/' + QString::number(mCharacter->maxHealth()));
//  mWeight->setText("<b>Weight</b>: " + QString::number(mCharacter->currentWeight())
//                   + '/' + QString::number(mCharacter->maxWeight()));
//  mLevel->setText("<b>Level</b>: " + QString::number(mCharacter->getCondition().currentLevel()));
//  mExpirience->setText("<b>Exp</b>: " + QString::number(mCharacter->getCondition().currentExpirience()));
//  mTime->setText(mCurrentTime->getTime());
}

void InfoWidget::setHealth(qint32 currentHealth, qint32 maxHealth)
{
  mHealth->setText("<b>Health</b>: " + QString::number(currentHealth)
                     + '/' + QString::number(maxHealth));
}

void InfoWidget::setWeight(qint32 currentWeight, qint32 maxWeight)
{
  mWeight->setText("<b>Weight</b>: " + QString::number(currentWeight)
                     + '/' + QString::number(maxWeight));
}

void InfoWidget::setLevel(qint32 level)
{
  mLevel->setText("<b>Level</b>: " + QString::number(level));
}

void InfoWidget::setExpirience(qint32 expirience)
{
  mExpirience->setText("<b>Exp</b>: " + QString::number(expirience));
}

void InfoWidget::setTime(QString time)
{
  mTime->setText(time);
}
