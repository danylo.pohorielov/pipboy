#ifndef SKILLSWIDGET_HPP
#define SKILLSWIDGET_HPP

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QListView>

#include "descriptionwidget.hpp"

class SkillsWidget : public DescriptionWidget
{
  Q_OBJECT
public:
  SkillsWidget(QWidget *parent = nullptr);

private:
  QLabel* mDescription;

  // DescriptionWidget interface
public:
  void fillInfo(Entity *entity) override;
};

#endif // SKILLSWIDGET_HPP
