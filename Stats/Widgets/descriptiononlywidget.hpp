#ifndef EFFECTSWIDGET_HPP
#define EFFECTSWIDGET_HPP

#include <QWidget>
#include <QListView>
#include <QGridLayout>
#include <QLabel>
#include <QStringList>

#include "descriptionwidget.hpp"


class DescriptionOnlyWidget : public DescriptionWidget
{
  Q_OBJECT
public:
  DescriptionOnlyWidget(QWidget *parent = nullptr);

  void fillInfo(Entity* item) override;

private:
  QLabel* mDescription;
};

#endif // EFFECTSWIDGET_HPP
