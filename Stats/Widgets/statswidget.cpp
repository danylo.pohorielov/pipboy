#include "Stats/Widgets/statswidget.hpp"
#include "Stats/Widgets/descriptiononlywidget.hpp"

StatsWidget::StatsWidget(Character* character, QWidget *parent) : QTabWidget(parent)
{
  mStatus = new StatusWidget(character);
  addTab(mStatus, "Status");

  mSpecial = new ListPictureWidget(new CharacteristicsWidget(this), character->getInventory().getItems("Weapons"));
  addTab(mSpecial, "Stats");

  mSkills = new ListPictureWidget(new SkillsWidget(this), character->getInventory().getItems("Weapons"));
  addTab(mSkills, "Skills");

  mPerks = new ListPictureWidget(new CharacteristicsWidget(this), character->getInventory().getItems("Weapons"));
  addTab(mPerks, "Perks");
}
