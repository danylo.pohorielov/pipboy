#include "Stats/Widgets/statuswidget.hpp"
#include "perkswidget.hpp"

StatusWidget::StatusWidget(Character* character, QWidget *parent) : QTabWidget(parent)
{
  setTabPosition(QTabWidget::West);
  mCondition = new ConditionWidget(character->getCondition());
  addTab(mCondition, "CND");

  mEffects = new ListPictureWidget(new PerksWidget(this), character->getInventory().getItems("Weapons"));
  addTab(mEffects, "EFF");
}
