#ifndef STATSWIDGET_H
#define STATSWIDGET_H

#include <QObject>
#include <QWidget>
#include <QTabWidget>

#include "listpicturewidget.hpp"
#include "statuswidget.hpp"
#include "characteristicswidget.hpp"
#include "skillswidget.hpp"
#include "perkswidget.hpp"

class StatsWidget : public QTabWidget
{
  Q_OBJECT
public:
  StatsWidget(Character* character, QWidget *parent = nullptr);

private:
  StatusWidget* mStatus;
  ListPictureWidget* mSpecial;
  ListPictureWidget* mSkills;
  ListPictureWidget* mPerks;
};

#endif // STATSWIDGET_H
