#ifndef CONDITIONWIDGET_HPP
#define CONDITIONWIDGET_HPP

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QProgressBar>
#include <QGraphicsDropShadowEffect>

#include "Stats/Back/condition.hpp"

class ConditionWidget : public QWidget
{
  Q_OBJECT
public:
  ConditionWidget(Condition* condition, QWidget *parent = nullptr);

  void applyShadowEffect(QWidget* widget);

  QLabel* mHunger;
  QLabel* mThirst;
  QLabel* mSleep;
  QLabel* mRadiation;

  QProgressBar* mHungerBar;
  QProgressBar* mThirstBar;
  QProgressBar* mSleepBar;
  QProgressBar* mRadiationBar;

  QGridLayout* mLayout;
  QGraphicsDropShadowEffect *shadowEffect;

  Condition* mCondition;
};

#endif // CONDITIONWIDGET_HPP
