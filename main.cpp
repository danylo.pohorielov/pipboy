//#include "mainwindow.hpp"
#include "Items/Widgets/pipboywidget.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  PipBoyWidget w;
  w.show();
  return a.exec();
}
