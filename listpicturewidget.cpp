#include "listpicturewidget.hpp"

#include <QPixmap>
#include <QHeaderView>

ListPictureWidget::ListPictureWidget(DescriptionWidget* descriptionWidget, CustomEntityModel* model, QWidget *parent)
  : QWidget{parent}, mModel(model), mDescription(descriptionWidget)
{
  mLayout = new QGridLayout(this);
  setLayout(mLayout);

  mView = new QTableView(this);

  mImage = new QLabel(this);
  mImage->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mImage->setMinimumSize(400, mView->size().height());

  mDescription->setParent(this);

  mLayout->addWidget(mView, 0, 0, 2, 1);
  mLayout->addWidget(mImage, 0, 1);
  mLayout->addWidget(mDescription, 1, 1);

  mView->setModel(mModel);
  mView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mView->setEditTriggers(QAbstractItemView::NoEditTriggers);
  mView->setMinimumWidth(400);

  mView->verticalHeader()->setVisible(false);
  mView->horizontalHeader()->setVisible(false);

  mView->setColumnWidth(0, 100);
  mView->setColumnWidth(1, 290);
  mView->setTextElideMode(Qt::ElideNone);

  mView->setMouseTracking(true);
  mView->setStyleSheet("QTableView::item:selected { border: 2px solid green; }"
                       "QTableView::item:hover { background-color: rgba(0, 255, 0, 100); }");

  connect(mView, &QTableView::entered, this, &ListPictureWidget::showInfo);
}

void ListPictureWidget::showImage(QString name)
{
  QString path(mPath + name.toLower() + ".png");
  QPixmap pixmap(path);
  //mImage->setStyleSheet("background-image: url" + path); //:/icons/resources/icons/knife.png
  mImage->setPixmap(pixmap);
  mImage->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  mImage->setStyleSheet("color: green;");
}

QTableView *ListPictureWidget::view() const
{
  return mView;
}

QGridLayout *ListPictureWidget::layout() const
{
  return mLayout;
}

CustomEntityModel *ListPictureWidget::model() const
{
  return mModel;
}

void ListPictureWidget::fillInfo(Entity *entity)
{
  mDescription->fillInfo(entity);
}

void ListPictureWidget::showInfo(const QModelIndex &index)
{
  mDescription->fillInfo(mModel->getEntity(index));
  showImage(mModel->getEntity(index)->name());
}
