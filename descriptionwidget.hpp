#ifndef DESCRIPTIONWIDGET_HPP
#define DESCRIPTIONWIDGET_HPP

#include <QWidget>
#include <QLabel>
#include <QGridLayout>
#include <Items/Back/item.hpp>

class DescriptionWidget : public QWidget
{
  Q_OBJECT
public:
  DescriptionWidget(QWidget *parent = nullptr);

  virtual void fillInfo(Entity* entity) = 0;

protected:
  QGridLayout* mLayout;
};

#endif // DESCRIPTIONWIDGET_HPP
