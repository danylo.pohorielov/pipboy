#ifndef ITEMDESCRIPTIONWIDGET_HPP
#define ITEMDESCRIPTIONWIDGET_HPP

#include "descriptionwidget.hpp"
#include <QWidget>


class ItemDescriptionWidget : public DescriptionWidget
{
  Q_OBJECT
public:
  ItemDescriptionWidget(QWidget *parent = nullptr);

  // DescriptionWidget interface
public:
  void fillInfo(Entity *entity) override;

  void fillStatsNSkills(Item *item);

protected:
  QLabel* mPrice;
  QLabel* mWeight;
  QLabel* mStats;
  QLabel* mSkills;
};

#endif // ITEMDESCRIPTIONWIDGET_HPP
