#include "miscwidget.hpp"
#include "Items/Back/misc.hpp"

MiscWidget::MiscWidget(QWidget *parent) : ItemDescriptionWidget{parent}
{
  mType             = new QLabel(this);
  mLayout->addWidget(mType);
}

void MiscWidget::fillInfo(Entity *item)
{
  ItemDescriptionWidget::fillInfo(item);
  Misc* misc = static_cast<Misc*>(item);
  mType->           setText("<b>Type:</b> " + gMiscTypeNames.at(misc->miscType()));
}
