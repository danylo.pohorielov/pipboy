#ifndef AMMOWIDGET_HPP
#define AMMOWIDGET_HPP

#include "itemdescriptionwidget.hpp"
#include <QWidget>

class AmmoWidget : public ItemDescriptionWidget
{
  Q_OBJECT
public:
  AmmoWidget(QWidget *parent = nullptr);

  void fillInfo(Entity* item) override;

private:
  QLabel* mDamageX;
};

#endif // AMMOWIDGET_HPP
