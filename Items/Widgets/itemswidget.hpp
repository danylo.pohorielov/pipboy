#ifndef ITEMSWIDGET_HPP
#define ITEMSWIDGET_HPP

#include <QWidget>
#include <QTabWidget>

#include "listpicturewidget.hpp"
#include "Items/Back/inventory.hpp"

class ItemsWidget : public QTabWidget
{
  Q_OBJECT
public:
  ItemsWidget(Inventory& inventory, QWidget *parent = nullptr);

public:
  ListPictureWidget* mWeapons;
  ListPictureWidget* mApparel;
  ListPictureWidget* mAid;
  ListPictureWidget* mMisc;
  ListPictureWidget* mAmmo;
};

#endif // ITEMSWIDGET_HPP
