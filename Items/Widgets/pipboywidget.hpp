#ifndef PIPBOYWIDGET_H
#define PIPBOYWIDGET_H

#include <QMainWindow>
#include <QObject>
#include <QTabWidget>
#include <QPixmap>
#include <QCursor>
#include <QStringListModel>

#include "Stats/Widgets/statswidget.hpp"
#include "Items/Widgets/itemswidget.hpp"
#include "Stats/Widgets/infowidget.hpp"
#include "character.hpp"
#include "time.hpp"
#include "shopwidget.hpp"

class PipBoyWidget : public QMainWindow
{
  Q_OBJECT
public:
  PipBoyWidget(QWidget *parent = nullptr);

//public slots:
//  void onHealthChanged();

//  void onWeightChanged();

//  void onExpirienceChanged();

//  void onTimeChanged();


private:
  QWidget* mMainWidget;
  QVBoxLayout* mLayout;
  QTabWidget* mMainWindow;

  InfoWidget* mInfo;
  StatsWidget* mStats;
  ItemsWidget* mItems;
  Character* mCharacter;
  const QCursor* mCursor;
  Time* mTime;
  ShopWidget* mShop;
  CustomEntityModel* mModel;
};

#endif // PIPBOYWIDGET_H
