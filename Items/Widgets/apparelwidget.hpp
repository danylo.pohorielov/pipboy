#ifndef APPARELWIDGET_H
#define APPARELWIDGET_H

#include "itemdescriptionwidget.hpp"
#include <QWidget>
#include "Items/Back/apparel.hpp"

class ApparelWidget : public ItemDescriptionWidget
{
  Q_OBJECT
public:
  ApparelWidget(QWidget *parent = nullptr);

  void fillInfo(Entity* item) override;

private:
  QLabel* mType;
  QLabel* mCondition;
  QLabel* mBaseDefense;
  QLabel* mPart;
  QLabel* mStats;
  QLabel* mSkills;
};

#endif // APPARELWIDGET_H
