#include "apparelwidget.hpp"

ApparelWidget::ApparelWidget(QWidget *parent) : ItemDescriptionWidget{parent}
{

  mType             = new QLabel(this);
  mCondition        = new QLabel(this);
  mBaseDefense      = new QLabel(this);
  mPart             = new QLabel(this);
  //mStats            = new QLabel(this);
  //mSkills           = new QLabel(this);

  mLayout->addWidget(mBaseDefense, 1, 0);
  mLayout->addWidget(mCondition, 1, 1);
  mLayout->addWidget(mType, 2, 0);
  mLayout->addWidget(mPart, 2, 1);

  //mStats->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
  //mSkills->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);

  //mLayout->addWidget(mStats, 2, 0);
  //mLayout->addWidget(mSkills, 2, 1);
}

void ApparelWidget::fillInfo(Entity *item)
{
  ItemDescriptionWidget::fillInfo(item);
  Apparel* apparel = static_cast<Apparel*>(item);
  double currentCondition = double(apparel->currentCondition()) / apparel->bestCondition()  * 100;
  mType->           setText("<b>Bodypart:</b> " + gApparelTypeNames.at(apparel->part()));
  mCondition->      setText("<b>Condition:</b> " + QString::number(currentCondition) + "%");
  mBaseDefense->    setText("<b>Defense:</b> " + QString::number(apparel->defense()));
//  if(apparel->effect() != nullptr)
//    {
//      mStats->show();
//      mSkills->show();
//      mStats->          setText("<b>Stats:</b><br>" + mapStats(apparel->effect()->getStatMods()));
//      mSkills->         setText("<b>Skills:</b><br>" + mapSkill(apparel->effect()->getSkillMods()));
//    }
//  else
//    {
//      mStats->hide();
//      mSkills->hide();
//    }
}

