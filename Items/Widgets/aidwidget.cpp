#include "aidwidget.hpp"

AidWidget::AidWidget(QWidget *parent) : ItemDescriptionWidget{parent}
{
  mType             = new QLabel(this);
  mRecovers         = new QLabel(this);

  mLayout->addWidget(mType, 1, 0);
  mLayout->addWidget(mRecovers, 1, 1);
}

void AidWidget::fillRecovers(Aid *aid)
{
  QString recovers;
  for(auto& index : aid->recovers())
    {
      recovers += gRecoveryTypeNames.at(index.first) + ": " + QString::number(index.second) + "<br>";
    }
  mRecovers->setText("<b>Recovers:</b><br>" + recovers);
}

void AidWidget::fillInfo(Entity *item)
{
  ItemDescriptionWidget::fillInfo(item);
  Aid* aid = static_cast<Aid*>(item);
  mType->           setText("<b>Type:</b> " + gAidTypeNames.at(aid->aidType()));
  fillRecovers(aid);
}
