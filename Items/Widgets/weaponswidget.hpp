#ifndef WEAPONSWIDGET_HPP
#define WEAPONSWIDGET_HPP

#include <QWidget>
#include <QLabel>
#include <QListWidget>
#include <QGridLayout>

#include "itemdescriptionwidget.hpp"
#include "Items/Back/item.hpp"

class WeaponsWidget : public ItemDescriptionWidget
{
  Q_OBJECT
public:
  explicit WeaponsWidget(QWidget *parent = nullptr);

private:
  QLabel* mType;
  QLabel* mCondition;
  QLabel* mBaseDamage;
  QLabel* mBullets;

public:
  void fillInfo(Entity* item) override;
};

#endif // WEAPONSWIDGET_HPP
