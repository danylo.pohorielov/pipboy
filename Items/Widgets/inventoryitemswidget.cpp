#include "inventoryitemswidget.hpp"

InventoryItemsWidget::InventoryItemsWidget(CustomEntityModel* model,
                                           QWidget* parent) :
  ListPictureWidget{nullptr, model, parent}
{
  //setData(mItems.getItemsNames());
}

void InventoryItemsWidget::setData(const QStringList& data)
{
  //mModel->setStringList(data);
}

Entity* InventoryItemsWidget::fillInfoByIndex(const QModelIndex &index)
{
  return mModel->getEntity(index);
}

void InventoryItemsWidget::showInfo(const QModelIndex &index)
{
  Entity* entity = fillInfoByIndex(index);
  if(entity != nullptr)
    fillInfo(entity);
}

//QString InventoryItemsWidget::mapStats(const StatMap &entries)
//{
//  if(entries.size() == 0)
//    return "";
//  QString data;
//  for(auto iter : entries) {
//      data += gStatNames.at(iter.first) + ": "
//          + (iter.second > 0 ? "+" : "") + QString::number(iter.second) + "<br>";
//    }
//  return data;
//}

//QString InventoryItemsWidget::mapSkill(const SkillMap &entries)
//{
//  if(entries.size() == 0)
//    return "";
//  QString data;
//  for(auto iter : entries) {
//      data += gSkillNames.at(iter.first) + ": "
//          + (iter.second > 0 ? "+" : "") + QString::number(iter.second) + "<br>";
//    }
//  return data;
//}
