#include "pipboywidget.hpp"

#include <QSizePolicy>
#include <QStyle>
#include "Items/Widgets/weaponswidget.hpp"

PipBoyWidget::PipBoyWidget(QWidget *parent) : QMainWindow(parent)
{
  //setStyleSheet("QMainWindow {background-color: #000000;color: #00FF00;border: 1px solid #00FF00;}");

  //change this shit
  QString styleSheet = "";
  styleSheet.append("QWidget {background-color: #000000;color: #0aad0a;font-family: \"Courier New\";"
                    "font-size: 20px;border: 1px solid #00FF00;}");
  styleSheet.append("QTabWidget::pane {background-color: #000000; border: 1px solid #00FF00;}");
  styleSheet.append("QTabBar::tab {background-color: #000000; color: #0aad0a; font-family: \"Courier New\";"
                    " font-size: 20px; border: 1px solid #00FF00; padding: 4px; min-height: 20px;}");
  styleSheet.append("QTabBar::tab:selected {border-color: #00FF00; background-color: #000000;}");
  styleSheet.append("QProgressBar {border: 2px black;border-radius: 5px;text-align: right;}");
  styleSheet.append("QProgressBar::chunk {background-color: #0aad0a;width: 10px;margin: 0.5px;}");
  setStyleSheet(styleSheet);

  mMainWidget           = new QWidget(this);
  mLayout               = new QVBoxLayout(this);
  mMainWindow           = new QTabWidget(this);
  mMainWindow->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

  mCharacter = new Character();
  //QWidget* temp = new QWidget();
  mModel = new CustomEntityModel(std::make_unique<WeaponReader>(":/txts/resources/data/weapons.txt"));
  mShop = new ShopWidget(
                         mCharacter->getInventory().getItems("Weapons"),
                         new ListPictureWidget(new WeaponsWidget(this), mModel, nullptr), nullptr);
  mShop->setStyleSheet(this->styleSheet());
  mShop->show();

  //const QPixmap* pixmap = new QPixmap("cursor2.png");
  //mCursor = new QCursor(*pixmap);

  mCharacter            = new Character();
  mStats                = new StatsWidget(mCharacter);
  mItems                = new ItemsWidget(mCharacter->getInventory(), this);
  mTime                 = new Time();
  mInfo                 = new InfoWidget(mCharacter, mTime, this);

  mCharacter->setUp();
  mTime->setUp();

  mMainWidget->setLayout(mLayout);
  mLayout->addWidget(mInfo);
  mLayout->addWidget(mMainWindow);

  //mInfo->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
  mInfo->setFixedHeight(50);

  mMainWindow->addTab(mStats, "Stats");
  mMainWindow->addTab(mItems, "Items");

  mMainWindow->setTabPosition(QTabWidget::South);

  setCentralWidget(mMainWidget);

  //setCursor(*mCursor);

  setMinimumWidth(1000);
  setMinimumHeight(600);

  //  connect(mCharacter, &Character::changeHealth, this, &PipBoyWidget::onHealthChanged);
}

//void PipBoyWidget::onHealthChanged()
//{
//  mInfo->setHealth(QString::number(mCharacter->currentHealth()) + '/' +
//                   QString::number(mCharacter->maxHealth()));
//}

//void PipBoyWidget::onWeightChanged()
//{
//  mInfo->setWeight(QString::number(mCharacter->currentWeight()) + '/' +
//                   QString::number(mCharacter->maxWeight()));
//}

//void PipBoyWidget::onExpirienceChanged()
//{
//  //mInfo->setExpirience(QString::number(mCharacter->getCondition().currentExpirience()));
//}

//void PipBoyWidget::onTimeChanged()
//{
//  mInfo->setTime(mTime->getTime());
//}
