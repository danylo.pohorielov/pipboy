#include "ammowidget.hpp"
#include "Items/Back/ammo.hpp"

AmmoWidget::AmmoWidget(QWidget *parent) : ItemDescriptionWidget{parent}
{
  mDamageX           = new QLabel(this);
  mLayout->addWidget(mDamageX);
}

void AmmoWidget::fillInfo(Entity *item)
{
  ItemDescriptionWidget::fillInfo(item);
  Ammo* ammo = static_cast<Ammo*>(item);
  mDamageX->        setText("<b>Damage:</b> " + QString::number(ammo->damageMultiplier()));
}
