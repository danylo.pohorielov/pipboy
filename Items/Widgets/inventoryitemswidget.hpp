#ifndef INVENTORYITEMSWIDGET_HPP
#define INVENTORYITEMSWIDGET_HPP

#include "listpicturewidget.hpp"
#include <QWidget>
#include <Stats/Back/stats.hpp>
#include <Stats/Back/skills.hpp>

class InventoryItemsWidget : public ListPictureWidget
{
  Q_OBJECT
public:
  InventoryItemsWidget(CustomEntityModel* entities, QWidget* parent = nullptr);

  void setData(const QStringList& data);

  Entity* fillInfoByIndex(const QModelIndex& index);

  virtual void fillInfo(Entity* entity) = 0;

public slots:

  void showInfo(const QModelIndex &index);

};

#endif // INVENTORYITEMSWIDGET_HPP
