#include "weaponswidget.hpp"
#include  <QPixmap>
#include "Items/Back/weapon.hpp"

WeaponsWidget::WeaponsWidget(QWidget *parent)
  : ItemDescriptionWidget{parent}
{
  mType             = new QLabel(this);
  mCondition        = new QLabel(this);
  mBaseDamage       = new QLabel(this);
  mBullets          = new QLabel(this);

  mLayout->addWidget(mType, 1, 0);
  mLayout->addWidget(mCondition, 1, 1);
  mLayout->addWidget(mBaseDamage, 2, 0);
  mLayout->addWidget(mBullets, 2, 1);
}

void WeaponsWidget::fillInfo(Entity* item)
{
  ItemDescriptionWidget::fillInfo(item);
  Weapon* weapon = static_cast<Weapon*>(item);
  double currentCondition = double(weapon->currentCondition()) / weapon->bestCondition()  * 100;
  mType->           setText("<b>Type:</b> " + gWeaponTypeNames.at(weapon->type()));
  mCondition->      setText("<b>Condition:</b> " + QString::number(currentCondition) + "%");
  mBaseDamage->     setText("<b>Damage:</b> " + QString::number(weapon->damage()));
  mBullets->        setText("<b>Bullets:</b> " + weapon->bullets());
}
