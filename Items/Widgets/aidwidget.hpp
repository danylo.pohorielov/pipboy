#ifndef AIDWIDGET_HPP
#define AIDWIDGET_HPP

#include <QObject>
#include <Items/Back/aid.hpp>
#include "itemdescriptionwidget.hpp"

// later change all {item name}Widget classes to DescriptionWidget : public QWidget class and a builder
class AidWidget : public ItemDescriptionWidget
{
  Q_OBJECT
public:
  AidWidget(QWidget *parent = nullptr);

  void fillRecovers(Aid* aid);

  void fillInfo(Entity* item) override;

private:
  QLabel* mType;
  QLabel* mRecovers;
};

#endif // AIDWIDGET_HPP
