#include "itemswidget.hpp"
#include "weaponswidget.hpp"
#include "apparelwidget.hpp"
#include "aidwidget.hpp"
#include "miscwidget.hpp"
#include "ammowidget.hpp"


ItemsWidget::ItemsWidget(Inventory& inventory, QWidget *parent) : QTabWidget{parent}
{
  mWeapons = new ListPictureWidget(new WeaponsWidget(this), inventory.getItems("Weapons"), this);
  addTab(mWeapons, "Weapons");

  mApparel = new ListPictureWidget(new ApparelWidget(this), inventory.getItems("Apparel"), this);
  addTab(mApparel, "Apparel");

  mAid = new ListPictureWidget(new AidWidget(this), inventory.getItems("Aid"), this);
  addTab(mAid, "Aid");

  mMisc = new ListPictureWidget(new MiscWidget(this), inventory.getItems("Misc"), this);
  addTab(mMisc, "Misc");

  mAmmo = new ListPictureWidget(new AmmoWidget(this), inventory.getItems("Ammo"), this);
  addTab(mAmmo, "Ammo");
}
