#ifndef MISCWIDGET_HPP
#define MISCWIDGET_HPP

#include "itemdescriptionwidget.hpp"
#include <QWidget>

class MiscWidget : public ItemDescriptionWidget
{
  Q_OBJECT
public:
  MiscWidget(QWidget *parent = nullptr);

  void fillInfo(Entity* item) override;

private:
  QLabel* mType;
};

#endif // MISCWIDGET_HPP
