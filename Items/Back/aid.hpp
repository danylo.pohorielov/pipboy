#ifndef AID_HPP
#define AID_HPP

#include "item.hpp"
#include <QWidget>
#include "Stats/Back/effects.hpp"

enum class AidType
{
  FOOD,
  WATER,
  CHEM,
  VENOMS,
  MISC
};

enum class RecoverType
{
  FOOD,
  WATER,
  SLEEP,
  RADIATION
};

static const std::map<AidType, QString> gAidTypeNames =
{
  {AidType::WATER,      "Water"},
  {AidType::FOOD,       "Food"},
  {AidType::CHEM,       "Chem"},
  {AidType::VENOMS,     "Venom"},
  {AidType::MISC,       "Misc"},
};

static const std::map<RecoverType, QString> gRecoveryTypeNames =
{
  {RecoverType::WATER,      "Water"},
  {RecoverType::FOOD,       "Food"},
  {RecoverType::SLEEP,      "Sleep"},
  {RecoverType::RADIATION,  "Radiation"},
};

class Aid : public Item
{
public:
  Aid();

  Aid(const QStringList& list);
  //Effect effect() const;
  void initBase(const QStringList& list);
  void readRecovery(const QStringList& list);

  AidType aidType() const;

  Effect* effect() const;

  const std::map<RecoverType, qint32>& recovers();

  void addSorts();

private:
  AidType mAidType;
  std::map<RecoverType, qint32> mRecovers;
  std::unique_ptr<Effect> mEffect;

  // Item interface
public:
  void use() override;
  void applyEffect() override;
  void drop() override;
  void sell() override;
};

class AidReader : public EntityReader
{
public:
  AidReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // AID_HPP
