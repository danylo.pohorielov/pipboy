#ifndef WEAPON_HPP
#define WEAPON_HPP

#include "item.hpp"
#include <QWidget>

enum class DamageType
{
  GUNS,
  ENERGY,
  MELEE,
  UNARMED,
  EXPLOSIVES
};

static const std::map<DamageType, QString> gWeaponTypeNames =
{
  {DamageType::GUNS,         "Guns"},
  {DamageType::ENERGY,       "Energy"},
  {DamageType::MELEE,        "Melee"},
  {DamageType::UNARMED,      "Unarmed"},
  {DamageType::EXPLOSIVES,   "Explosive"},
};

class Weapon : public Item
{
public:
  Weapon();

  Weapon(const QStringList& list);

  DamageType type() const;
  void setType(DamageType newType);
  qint32 damage() const;
  void setDamage(qint32 newDamage);
  QString bullets() const;
  void setBullets(const QString &newBullets);
  qint32 strengthRequired() const;
  void setStrengthRequired(qint32 newStrengthRequired);
  qint32 skillRequired() const;
  void setSkillRequired(qint32 newSkillRequired);
  qint32 currentCondition() const;
  void setCurrentCondition(qint32 condition);
  qint32 bestCondition() const;
  qint32 damageCube() const;
  void setDamageCube(qint32 newDamageCube);
  DamageType damageType() const;
  qint32 condition();


  void addSorts();

private:
  DamageType mDamageType;
  qint32 mDamageCube;
  qint32 mDamage;
  QString mBullets = "";
  qint32 mStrengthRequired;
  qint32 mSkillRequired;
  //qint32 mCurrentCondition;
  qint32 mBestCondition;

  // Item interface
public:
  void use() override;
  void applyEffect() override;
  void drop() override;
  void sell() override;
  qint32 amount() const override;
};

class WeaponReader : public EntityReader
{
public:
  WeaponReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // WEAPON_HPP
