#ifndef APPAREL_HPP
#define APPAREL_HPP

#include "item.hpp"
#include "Stats/Back/effects.hpp"

enum class Bodypart
{
  HEAD,
  TORSO,
  RIGHT_ARM,
  LEFT_ARM,
  RIGHT_LEG,
  LEFT_LEG
};

static const std::map<Bodypart, QString> gApparelTypeNames =
{
  {Bodypart::HEAD,      "Head"},
  {Bodypart::TORSO,     "Torso"},
  {Bodypart::RIGHT_ARM, "Right arm"},
  {Bodypart::LEFT_ARM,  "Left arm"},
  {Bodypart::RIGHT_LEG, "Right leg"},
  {Bodypart::LEFT_LEG,  "Left leg"},
};

class Apparel : public Item
{
public:
  Apparel();

  Apparel(const QStringList& list);

  void initBase(const QStringList& list);

  qint32 defense() const;
  Bodypart part() const;

  qint32 currentCondition() const;

  qint32 bestCondition() const;

  qint32 amount() const override;

  qint32 condition();

  void addSorts();

private:
  Bodypart mPart;
  qint32 mBaseDefense;
  qint32 mModifiedDefense;
  qint32 mStrengthRequired;
  qint32 mBestCondition;

  // Item interface
public:
  void use() override;
  void applyEffect() override;
  void drop() override;
  void sell() override;
  qint32 modifiedDefense() const;
  qint32 strengthRequired() const;
};

class ApparelReader : public EntityReader
{
public:
  ApparelReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // APPAREL_HPP
