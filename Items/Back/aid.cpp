#include "aid.hpp"

Aid::Aid() : Item{}
{
  mType = Type::AID;
}

Aid::Aid(const QStringList& list) : Item{}
{
  mType = Type::AID;
  qint32 index = 0;
  initBase(list[index++].split(';'));
  readRecovery(list[index++].split(';'));
  if(!list[index].isEmpty())
    {
      mEffect = std::make_unique<Effect>(list[index++].split('#'));
    }
  else
    {
      mEffect = nullptr;
    }
  addSorts();
}


void Aid::initBase(const QStringList &list)
{
  qint32 index = 0;
  mName = list[index++];
  mPrice = list[index++].toInt();
  mWeight = list[index++].toInt();
  mCurrentCondition = list[index++].toInt();
  mAidType = AidType(list[index++].toInt());
}

void Aid::readRecovery(const QStringList &list)
{
  QStringList temp;
  for (qint32 i = 0; i < list.size(); ++i)
    {
      temp = list[i].split(' ');
      mRecovers[RecoverType(temp[0].toInt())] = temp[1].toInt();
    }

}

AidType Aid::aidType() const
{
  return mAidType;
}

Effect*Aid::effect() const
{
  return mEffect.get();
}

const std::map<RecoverType, qint32>& Aid::recovers()
{
  return mRecovers;
}

void Aid::addSorts()
{
  mSorts.insert({"AidType", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Aid*>(lhs)->aidType() < static_cast<Aid*>(rhs)->aidType();}
                });
}

void Aid::use()
{

}

void Aid::applyEffect()
{

}

void Aid::drop()
{

}

void Aid::sell()
{

}

//Effect Aid::effect() const
//{
//  return mEffect;
//}


AidReader::AidReader(const QString &filename) : EntityReader{filename}
{

}

std::unique_ptr<Entity> AidReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Aid>(line.split('|'));
}
