#ifndef MISC_HPP
#define MISC_HPP

#include "item.hpp"
#include <QWidget>

enum class MiscType
{
  CURRENCY,
  PART,
  JUNK,
};

static const std::map<MiscType, QString> gMiscTypeNames =
{
  {MiscType::CURRENCY,  "Currency"},
  {MiscType::PART,      "Part"},
  {MiscType::JUNK,      "Junk"},
};

class Misc : public Item
{
public:
  Misc();

  Misc(const QStringList& list);

  MiscType miscType() const;

  void addSorts();


private:
  MiscType mMiscType;

  // Item interface
public:
  void use() override;
  void applyEffect() override;
  void drop() override;
  void sell() override;
};

class MiscReader : public EntityReader
{
public:
  MiscReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // MISC_HPP
