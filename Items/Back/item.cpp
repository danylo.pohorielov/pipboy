#include "item.hpp"
#include <QDebug>
#include <QDir>

Item::Item(QString name, float weight, qint32 price)
  : Entity(name, weight, price)
{
  addSorts();
}

Type Item::type() const
{
  return mType;
}

void Item::addSorts()
{
  mSorts =
  {
    {"Name",    [](Item* lhs, Item* rhs){ return lhs->name() < rhs->name();}},
    {"Weight",  [](Item* lhs, Item* rhs){ return lhs->weight() < rhs->weight();}},
    {"Price",   [](Item* lhs, Item* rhs){ return lhs->price() < rhs->price();}},
    {"Type",    [](Item* lhs, Item* rhs){ return lhs->type() < rhs->type();}}
  };
}

Effect* Item::effect() const
{
  return mEffect.get();
}
