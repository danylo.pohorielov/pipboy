#ifndef ITEM_HPP
#define ITEM_HPP

#include <QWidget>
#include <QFile>
#include <QTextStream>

#include <functional>

#include "entity.hpp"
#include "Stats/Back/effects.hpp"

enum class Type{
  WEAPON,
  APPAREL,
  AID,
  MISC,
  AMMO
};

static const std::map<Type, QString> gTypeNames =
{
  {Type::WEAPON,    "Weapon"},
  {Type::APPAREL,   "Apparel"},
  {Type::AID,       "Aid"},
  {Type::MISC,      "Misc"},
  {Type::AMMO,      "Ammo"},
};

class Item : public Entity
{
public:

  //Item() = default;

  Item(QString name = "", float weight = 0, qint32 price = 0);

  Type type() const;

  Effect* effect() const;

  virtual void use() = 0;

  virtual void applyEffect() = 0;

  virtual void drop() = 0;

  virtual void sell() = 0;

  void addSorts();

protected:
  Type mType;
  std::unique_ptr<Effect> mEffect;
  std::map<QString, std::function<bool(Item*, Item*)>> mSorts;
};

//Q_DECLARE_METATYPE(Item)



#endif // ITEM_HPP
