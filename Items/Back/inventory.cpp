#include "inventory.hpp"

Inventory::Inventory()
{
  countWeight();
}

CustomEntityModel *Inventory::getItems(QString name)
{
  return mItems.at(name);
}

double Inventory::countWeight()
{
  double weight = 0;
  for(auto& inventory : mItems)
    {
      weight += inventory.second->currentWeight();
    }
  return weight;
}

void Inventory::addItem(Item *item)
{
  //mItems.at(gTypeNames.at(item->type()))
}

void Inventory::useItem(Item *item)
{
    
}

void Inventory::applyEffect(Item *item)
{
    
}

void Inventory::dropItem(Item *item)
{
    
}

void Inventory::sellItem(Item *item)
{
    
}

void Inventory::removeItem(Item *item)
{
    
}
