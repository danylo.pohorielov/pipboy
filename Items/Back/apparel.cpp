#include "apparel.hpp"

Apparel::Apparel() : Item{}
{
  mType = Type::APPAREL;
}

Apparel::Apparel(const QStringList& list) : Item{}
{
  qint32 index = 0;
  initBase(list[index++].split(';'));
  if(!list[index].isEmpty())
    {
      mEffect = std::make_unique<Effect>(list[index++].split('#'));
    }
  else
    {
      mEffect = nullptr;
    }
}

void Apparel::initBase(const QStringList &list)
{
  mType = Type::APPAREL;
  qint32 index = 0;
  mName             = list[index++];
  mPrice            = list[index++].toInt();
  mWeight           = list[index++].toInt();
  mPart             = Bodypart(list[index++].toInt());
  mBaseDefense      = list[index++].toInt();
  mStrengthRequired = list[index++].toInt();
  mCurrentCondition = list[index++].toInt();
  mBestCondition    = list[index++].toInt();
  addSorts();
}

qint32 Apparel::defense() const
{
  return mBaseDefense;
}

Bodypart Apparel::part() const
{
  return mPart;
}

qint32 Apparel::currentCondition() const
{
  return mCurrentCondition;
}

qint32 Apparel::bestCondition() const
{
  return mBestCondition;
}

void Apparel::addSorts()
{
  mSorts.insert({"Part", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Apparel*>(lhs)->part()
                   < static_cast<Apparel*>(rhs)->part();}
                });
  mSorts.insert({"ModifiedDefense", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Apparel*>(lhs)->modifiedDefense()
                   < static_cast<Apparel*>(rhs)->modifiedDefense();}
                });
  mSorts.insert({"Condition", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Apparel*>(lhs)->condition()
                   < static_cast<Apparel*>(rhs)->condition();}
                });
}

qint32 Apparel::modifiedDefense() const
{
  return mModifiedDefense;
}

qint32 Apparel::strengthRequired() const
{
  return mStrengthRequired;
}

void Apparel::use()
{

}

void Apparel::applyEffect()
{

}

void Apparel::drop()
{

}

void Apparel::sell()
{

}

qint32 Apparel::amount() const
{
  return 1;
}

qint32 Apparel::condition()
{
  return mCurrentCondition / mBestCondition * 100;
}

ApparelReader::ApparelReader(const QString &filename)
  : EntityReader{filename}
{
    
}

std::unique_ptr<Entity> ApparelReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Apparel>(line.split('|'));
}
