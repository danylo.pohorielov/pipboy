#include "ammo.hpp"

Ammo::Ammo() : Item{}
{
  mType = Type::AMMO;
}

Ammo::Ammo(const QStringList& list) : Item{}
{
  mType = Type::AMMO;
  qint32 index = 0;
  mName = list[index++];
  mPrice = list[index++].toInt();
  mWeight = list[index++].toInt();
  mCurrentCondition = list[index++].toInt();
  mDamageMultiplier = list[index++].toInt();
  mAmmoType = list[index++];
  addSorts();
}

float Ammo::damageMultiplier() const
{
  return mDamageMultiplier;
}

QString Ammo::name() const
{
  return mName + ' ' + mAmmoType;
}

QString Ammo::ammoType() const
{
  return mAmmoType;
}

void Ammo::addSorts()
{
  mSorts.insert({"Amount", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Ammo*>(lhs)->amount()
                   < static_cast<Ammo*>(rhs)->amount();}
                });
  mSorts.insert({"DamageMultiplier", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Ammo*>(lhs)->damageMultiplier()
                   < static_cast<Ammo*>(rhs)->damageMultiplier();}
                });
  mSorts.insert({"AmmoType", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Ammo*>(lhs)->ammoType()
                   < static_cast<Ammo*>(rhs)->ammoType();}
                });
}

void Ammo::use()
{

}

void Ammo::applyEffect()
{

}

void Ammo::drop()
{

}

void Ammo::sell()
{

}


AmmoReader::AmmoReader(const QString &filename) : EntityReader{filename}
{

}

std::unique_ptr<Entity> AmmoReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Ammo>(line.split(';'));
}
