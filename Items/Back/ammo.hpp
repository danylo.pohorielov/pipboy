#ifndef AMMO_HPP
#define AMMO_HPP

#include "item.hpp"
#include <QWidget>

class Ammo : public Item
{
public:
  Ammo();

  Ammo(const QStringList& list);

  float damageMultiplier() const;

  QString name() const override;

  QString ammoType() const;

  void addSorts();

private:
  float mDamageMultiplier;
  QString mAmmoType;

  // Item interface
public:
  void use() override;
  void applyEffect() override;
  void drop() override;
  void sell() override;
};

class AmmoReader : public EntityReader
{
public:
  AmmoReader(const QString &filename);
  std::unique_ptr<Entity> readEntry();
};

#endif // AMMO_HPP
