#include "misc.hpp"

Misc::Misc() : Item{}
{
  mType = Type::MISC;
}

Misc::Misc(const QStringList &list) : Item{}
{
  mType = Type::MISC;
  qint32 index = 0;
  mName = list[index++];
  mPrice = list[index++].toInt();
  mWeight = list[index++].toInt();
  mCurrentCondition = list[index++].toInt();
  mMiscType = MiscType(list[index++].toInt());
  addSorts();
}

MiscType Misc::miscType() const
{
  return mMiscType;
}

void Misc::addSorts()
{
  mSorts.insert({"Amount", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Misc*>(lhs)->amount()
                   < static_cast<Misc*>(rhs)->amount();}
                });
  mSorts.insert({"MiscType", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Misc*>(lhs)->miscType()
                   < static_cast<Misc*>(rhs)->miscType();}
                });
}

void Misc::use()
{

}

void Misc::applyEffect()
{

}

void Misc::drop()
{

}

void Misc::sell()
{

}


MiscReader::MiscReader(const QString &filename) : EntityReader{filename}
{

}

std::unique_ptr<Entity> MiscReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Misc>(line.split(';'));
}
