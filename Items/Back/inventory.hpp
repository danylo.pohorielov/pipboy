#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include <map>

#include <QWidget>

//#include "itemsmanager.hpp"
#include "weapon.hpp"
#include "apparel.hpp"
#include "aid.hpp"
#include "misc.hpp"
#include "ammo.hpp"
#include "customentitymodel.hpp"

class Inventory
{
public:
  Inventory();

  CustomEntityModel* getItems(QString name);

  double countWeight();

  void addItem(Item* item);

  void useItem(Item* item);

  void applyEffect(Item* item);

  void dropItem(Item* item);

  void sellItem(Item* item);

  void removeItem(Item* item);

private:
  const std::map<QString, CustomEntityModel*> mItems =
    {
      {"Weapons", new CustomEntityModel{
         std::make_unique<WeaponReader>(":/txts/resources/data/weapons.txt")}},
      {"Apparel", new CustomEntityModel{
         std::make_unique<ApparelReader>(":/txts/resources/data/apparel.txt")}},
      {"Aid",     new CustomEntityModel{
         std::make_unique<AidReader>(":/txts/resources/data/aid.txt")}},
      {"Misc",    new CustomEntityModel{
         std::make_unique<MiscReader>(":/txts/resources/data/misc.txt")}},
      {"Ammo",    new CustomEntityModel{
         std::make_unique<AmmoReader>(":/txts/resources/data/ammo.txt")}}
    };
};

#endif // INVENTORY_HPP
