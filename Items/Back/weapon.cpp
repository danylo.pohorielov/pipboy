#include "weapon.hpp"

Weapon::Weapon() : Item{}
{
  mType = Type::WEAPON;
}

Weapon::Weapon(const QStringList &list)
{
  mType = Type::WEAPON;
  qint32 index = 0;
  mName             = list[index++];
  mPrice            = list[index++].toInt();
  mWeight           = list[index++].toInt();
  mDamageType       = DamageType(list[index++].toInt());
  mDamageCube       = list[index++].toInt();
  mDamage           = list[index++].toInt();
  if(mDamageType != DamageType::MELEE && mDamageType != DamageType::UNARMED)
    mBullets        = list[index++];
  mStrengthRequired = list[index++].toInt();
  mSkillRequired    = list[index++].toInt();
  mCurrentCondition = list[index++].toInt();
  mBestCondition    = list[index++].toInt();
  addSorts();
}

DamageType Weapon::type() const
{
  return mDamageType;
}

void Weapon::setType(DamageType newType)
{
  mDamageType = newType;
}

qint32 Weapon::damage() const
{
  return mDamage;
}

void Weapon::setDamage(qint32 newDamage)
{
  mDamage = newDamage;
}

QString Weapon::bullets() const
{
  return mBullets;
}

void Weapon::setBullets(const QString &newBullets)
{
  mBullets = newBullets;
}

qint32 Weapon::strengthRequired() const
{
  return mStrengthRequired;
}

void Weapon::setStrengthRequired(qint32 newStrengthRequired)
{
  mStrengthRequired = newStrengthRequired;
}

qint32 Weapon::skillRequired() const
{
  return mSkillRequired;
}

void Weapon::setSkillRequired(qint32 newSkillRequired)
{
  mSkillRequired = newSkillRequired;
}

qint32 Weapon::currentCondition() const
{
  return mCurrentCondition;
}

void Weapon::setCurrentCondition(qint32 condition)
{
  mCurrentCondition = condition;
}

qint32 Weapon::bestCondition() const
{
  return mBestCondition;
}

qint32 Weapon::damageCube() const
{
  return mDamageCube;
}

void Weapon::setDamageCube(qint32 newDamageCube)
{
  mDamageCube = newDamageCube;
}

//mDamage
//if(mDamageType !=
//  mBullets
//mStrengthRequired
//mSkillRequired
//mCurrentCondition
//mBestCondition

void Weapon::addSorts()
{
  mSorts.insert({"DamageType", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Weapon*>(lhs)->damageType()
                   < static_cast<Weapon*>(rhs)->damageType();}
                });
  mSorts.insert({"DamageCube", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Weapon*>(lhs)->damageCube()
                   < static_cast<Weapon*>(rhs)->damageCube();}
                });
  mSorts.insert({"Damage", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Weapon*>(lhs)->damage()
                   < static_cast<Weapon*>(rhs)->damage();}
                });
  if(!mBullets.isEmpty())
    {
      mSorts.insert({"ModifiedDefense", [](Item* lhs, Item* rhs)
                     {
                       return static_cast<Weapon*>(lhs)->bullets()
                       < static_cast<Weapon*>(rhs)->bullets();}
                    });
    }
  mSorts.insert({"Strength", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Weapon*>(lhs)->strengthRequired()
                   < static_cast<Weapon*>(rhs)->strengthRequired();}
                });
  mSorts.insert({"Skill", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Weapon*>(lhs)->skillRequired()
                   < static_cast<Weapon*>(rhs)->skillRequired();}
                });
  mSorts.insert({"Skill", [](Item* lhs, Item* rhs)
                 {
                   return static_cast<Weapon*>(lhs)->condition()
                   < static_cast<Weapon*>(rhs)->condition();}
                });
}

DamageType Weapon::damageType() const
{
  return mDamageType;
}

qint32 Weapon::condition()
{
  return mCurrentCondition / mBestCondition * 100;
}

void Weapon::use()
{

}

void Weapon::applyEffect()
{

}

void Weapon::drop()
{

}

void Weapon::sell()
{

}

qint32 Weapon::amount() const
{
  return 1;
}

WeaponReader::WeaponReader(const QString &filename)
  : EntityReader{filename}
{

}

std::unique_ptr<Entity> WeaponReader::readEntry()
{
  QString line;
  if (!mFileReader->atEnd())
  {
    line = mFileReader->readLine();
  }
  return std::make_unique<Weapon>(line.split(' '));
}
