QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Items/Back/aid.cpp \
    Items/Back/ammo.cpp \
    Items/Back/apparel.cpp \
    Items/Back/misc.cpp \
    Stats/Back/skill.cpp \
    Stats/Back/skills.cpp \
    Stats/Back/stat.cpp \
    Stats/Back/stats.cpp \
    Stats/Widgets/characteristicswidget.cpp \
    Stats/Widgets/descriptiononlywidget.cpp \
    Stats/Widgets/infowidget.cpp \
    Stats/Widgets/statswidget.cpp \
    Stats/Widgets/statuswidget.cpp \
    Stats/Widgets/conditionwidget.cpp \
    Stats/Widgets/skillswidget.cpp \
    Stats/Widgets/perkswidget.cpp \
    Stats/Back/condition.cpp \
    Stats/Back/effects.cpp \
    Stats/Back/perks.cpp \
    Stats/Back/radiation.cpp \
    Stats/Back/serializable.cpp \
    character.cpp \
    Items/Back/inventory.cpp \
    Items/Widgets/inventoryitemswidget.cpp \
    Items/Back/item.cpp \
    customentitymodel.cpp \
    descriptionwidget.cpp \
    entity.cpp \
    globaltems.cpp \
    info.cpp \
    itemdescriptionwidget.cpp \
    listpicturewidget.cpp \
    lpwwithstatsnskils.cpp \
    main.cpp \
    Items/Widgets/itemswidget.cpp \
    Items/Widgets/aidwidget.cpp \
    Items/Widgets/ammowidget.cpp \
    Items/Widgets/apparelwidget.cpp \
    Items/Widgets/miscwidget.cpp \
    Items/Widgets/weaponswidget.cpp \
    Items/Widgets/pipboywidget.cpp \
    Items/Back/weapon.cpp \
    shopwidget.cpp \
    time.cpp

HEADERS += \
    Items/Back/aid.hpp \
    Items/Back/ammo.hpp \
    Items/Back/apparel.hpp \
    Items/Back/misc.hpp \
    Stats/Back/skill.hpp \
    Stats/Back/skills.hpp \
    Stats/Back/stat.hpp \
    Stats/Back/stats.hpp \
    Stats/Widgets/characteristicswidget.hpp \
    Stats/Widgets/descriptiononlywidget.hpp \
    Stats/Widgets/infowidget.hpp \
    Stats/Widgets/statswidget.hpp \
    Stats/Widgets/statuswidget.hpp \
    Stats/Widgets/conditionwidget.hpp \
    Stats/Widgets/skillswidget.hpp \
    Stats/Widgets/perkswidget.hpp \
    Stats/Back/radiation.hpp \
    Stats/Back/condition.hpp \
    Stats/Back/effects.hpp \
    Stats/Back/perks.hpp \
    Stats/Back/serializable.hpp \
    character.hpp \
    Items/Back/inventory.hpp \
    Items/Widgets/inventoryitemswidget.hpp \
    Items/Back/item.hpp \
    customentitymodel.hpp \
    descriptionwidget.hpp \
    entity.hpp \
    globaltems.hpp \
    info.hpp \
    itemdescriptionwidget.hpp \
    listpicturewidget.hpp \
    Items/Widgets/apparelwidget.hpp \
    Items/Widgets/ammowidget.hpp \
    Items/Widgets/aidwidget.hpp \
    Items/Widgets/itemswidget.hpp \
    Items/Widgets/miscwidget.hpp \
    Items/Widgets/weaponswidget.hpp \
    Items/Widgets/pipboywidget.hpp \
    Items/Back/weapon.hpp \
    lpwwithstatsnskils.hpp \
    shopwidget.hpp \
    time.hpp

RESOURCES     =     Resources.qrc


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
