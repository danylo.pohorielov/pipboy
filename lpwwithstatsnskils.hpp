#ifndef LPWWITHSTATSNSKILS_HPP
#define LPWWITHSTATSNSKILS_HPP

#include "listpicturewidget.hpp"
#include "Stats/Back/effects.hpp"

class LPWWithStatsNSkils : public ListPictureWidget
{
  Q_OBJECT
public:
  LPWWithStatsNSkils(DescriptionWidget* descriptionWidget,
                     CustomEntityModel* model, QWidget* parent = nullptr);

  void showStats(StatMap& stats);

  void showSkills(SkillMap& skills);

private:
  QLabel* mStats;
  QLabel* mSkills;
};

#endif // LPWWITHSTATSNSKILS_HPP
