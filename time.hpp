#ifndef TIME_HPP
#define TIME_HPP

#include <QDateTime>
#include <QObject>
#include <QLocale>

class Time : public QObject
{
  Q_OBJECT
public:
  Time();

  QString getTime();

  void changeTime(qint32 value);

  void setTime(QString value);

  void setUp();

signals:
  void timeChanged(QString value);

private:
  QDateTime mTime;
  QString mFormat = "yyyy/MM/dd HH:mm";
  QLocale mLocale;
};

#endif // TIME_HPP
