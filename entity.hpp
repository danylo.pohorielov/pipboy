#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <QString>
#include <QTextStream>
#include <QFile>
#include <memory>

class Entity
{
public:
  Entity(QString name = " ", qint32 weight = 0, qint32 price = 0, qint32 amount = 0);

  virtual QString name() const;
  void setName(const QString &newName);

  float weight() const;
  void setWeight(float newWeight);
  qint32 price() const;
  void setPrice(qint32 newPrice);
  virtual qint32 amount() const;
  void setAmount(qint32 newAmount);

  QString imageName() const;
  void setImageName(const QString &newImageName);

protected:
  QString mName;
  QString mImageName;
  float mWeight;
  qint32 mPrice;
  qint32 mCurrentCondition;
};

class EntityReader
{
public:
  EntityReader(const QString& filename);

  virtual std::unique_ptr<Entity> readEntry() = 0;

  std::unique_ptr<std::vector<std::unique_ptr<Entity>>> readFile();

  virtual ~EntityReader();

protected:
  QFile mFile;
  std::unique_ptr<QTextStream> mFileReader;
};

#endif // ENTITY_HPP
