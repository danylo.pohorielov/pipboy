#include "entity.hpp"
#include <QDebug>
#include <QDir>

Entity::Entity(QString name, qint32 weight, qint32 price, qint32 amount)
  : mName(name), mWeight(weight), mPrice(price), mCurrentCondition(amount)
{

}

QString Entity::name() const
{
  return mName;
}

void Entity::setName(const QString &name)
{
  mName = name;
}

float Entity::weight() const
{
  return mWeight;
}

void Entity::setWeight(float weight)
{
  mWeight = weight;
}

qint32 Entity::price() const
{
  return mPrice;
}

void Entity::setPrice(qint32 price)
{
  mPrice = price;
}

qint32 Entity::amount() const
{
  return mCurrentCondition;
}

void Entity::setAmount(qint32 currentCondition)
{
  mCurrentCondition = currentCondition;
}

QString Entity::imageName() const
{
  return mImageName;
}

void Entity::setImageName(const QString &newImageName)
{
  mImageName = newImageName;
}

EntityReader::EntityReader(const QString& filename)
  : mFile{filename}//mFile{std::make_unique<QFile>(filename)}
{
  qDebug() << QDir::currentPath();
  if(!mFile.open(QIODevice::ReadOnly | QIODevice::Text))
    throw "Couldn't open file";
  mFileReader = std::make_unique<QTextStream>(&mFile);
}

std::unique_ptr<std::vector<std::unique_ptr<Entity> > > EntityReader::readFile()
{
  std::unique_ptr<std::vector<std::unique_ptr<Entity>>> owner
      = std::make_unique<std::vector<std::unique_ptr<Entity>>>();
  while(!mFileReader->atEnd())
    {
      owner->push_back(readEntry());
    }
  return owner;
}

EntityReader::~EntityReader()
{

}
