#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include <QWidget>
#include "Stats/Back/perks.hpp"
#include "Items/Back/inventory.hpp"
#include "Stats/Back/condition.hpp"


// make a file
namespace QConstants
{
  static const qint32 MAX_XP = 116250;
  static const qint32 MAX_LEVEL = 30;
  static const qint32 START_XP = 250;
  static const qint32 ADDITIONAL_XP_PER_LEVEL = 250;

  static const qint32 HP_PER_ENDURANCE = 10;
  static const qint32 WEIGHT_PER_ENDURANCE = 10;

  static const qint32 BASE_WEIGHT = 50;
  static const qint32 BASE_HEALTH = 50;
}

class Character : public QObject
{
  Q_OBJECT
public:
  Character();

  Inventory& getInventory();

  Condition* getCondition();

  void setUp();

  qint32 currentHealth() const;
  qint32 maxHealth() const;
  double currentWeight() const;
  double maxWeight() const;
  qint32 currentLevel() const;
  qint32 currentExpirience() const;

  void changeHealth(qint32 value);

  void changeWeight(double newCurrentWeight);

  void changeExpirience(qint32 value);

  void changeLevel();

  void changeStats(StatMap& value);

  void changeSkills(SkillMap& value);

signals:

  void healthChanged(qint32 currentHealth, qint32 maxHealth);

  void weightChanged(double currentWeight, double maxWeight);

  void expirienceChanged(qint32 value);

  void levelChanged(qint32 value);

  void statsChanged(const StatMap& value);

  void skillsChanged(const SkillMap& value);

private:

  qint32 mCurrentHealth = 0;
  qint32 mMaxHealth = 0;
  double mCurrentWeight = 0;
  double mMaxWeight = 0;
  qint32 mCurrentLevel = 1;
  qint32 mCurrentExpirience = 0;
  Condition mCondition;
  Characteristics mStats;
  Abilities mSkills;
  Perks mPerks;
  Inventory mInventory;
};

#endif // CHARACTER_HPP
