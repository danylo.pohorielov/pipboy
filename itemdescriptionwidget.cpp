#include "itemdescriptionwidget.hpp"

ItemDescriptionWidget::ItemDescriptionWidget(QWidget *parent)
  : DescriptionWidget{parent}
{
  mPrice  = new QLabel(this);
  mWeight = new QLabel(this);
  mStats  = new QLabel(this);
  mSkills = new QLabel(this);

  mStats->hide();
  mSkills->hide();

  mLayout->addWidget(mPrice, 0, 0);
  mLayout->addWidget(mWeight, 0, 1);
  mLayout->addWidget(mStats, 1, 0);
  mLayout->addWidget(mSkills, 1, 1);
}

void ItemDescriptionWidget::fillInfo(Entity *entity)
{
  Item* item = static_cast<Item*>(entity);
  fillStatsNSkills(item);
  mWeight->         setText("<b>Weight:</b> " + QString::number(item->weight()));
  mPrice->          setText("<b>Price:</b> " + QString::number(item->price()));
}

void ItemDescriptionWidget::fillStatsNSkills(Item *item)
{
  Effect* effect = item->effect();
  if(effect != nullptr)
    {
      mStats->show();
      mSkills->show();
      mStats->setText("<b>Stats:</b> " + effect->mapStats());
      mSkills->setText("<b>Skills:</b> " + effect->mapSkills());
    }
  else
    {
      mStats->hide();
      mSkills->hide();
    }
}
