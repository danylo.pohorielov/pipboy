#include "time.hpp"

Time::Time() : mTime(), mLocale(QLocale::English, QLocale::UnitedStates)
{
  setTime("2077/08/13 19:20");
}

QString Time::getTime()
{
  return mTime.toString(mFormat);
}

void Time::changeTime(qint32 value)
{
  mTime = mTime.addSecs(value * 3600);
  emit timeChanged(mTime.toString());
}

void Time::setTime(QString value)
{
  mTime = mTime.fromString(value, mFormat);
}

void Time::setUp()
{
  emit timeChanged(mLocale.toString(mTime, mFormat));
}
