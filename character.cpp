#include "character.hpp"

Character::Character()
{
  mMaxHealth = QConstants::BASE_HEALTH + mStats.at(Stats::ENDURANCE) * QConstants::HP_PER_ENDURANCE;
  mMaxWeight = QConstants::BASE_WEIGHT + mStats.at(Stats::ENDURANCE) * QConstants::WEIGHT_PER_ENDURANCE;
  changeHealth(mMaxHealth);
  changeWeight(mInventory.countWeight());
}


Inventory& Character::getInventory()
{
  return mInventory;
}

Condition *Character::getCondition()
{
  return &mCondition;
}

void Character::setUp()
{
  emit healthChanged(mCurrentHealth, mMaxHealth);
  emit weightChanged(mCurrentWeight, mMaxWeight);
  emit levelChanged(mCurrentLevel);
  emit expirienceChanged(mCurrentExpirience);
  emit statsChanged(mStats.getStats());
  emit skillsChanged(mSkills.getSkills());
  mCondition.setUp();
}

qint32 Character::currentHealth() const
{
  return mCurrentHealth;
}

qint32 Character::maxHealth() const
{
  return mMaxHealth;
}

double Character::currentWeight() const
{
  return mCurrentWeight;
}

qint32 Character::currentLevel() const
{
  return mCurrentLevel;
}

qint32 Character::currentExpirience() const
{
  return mCurrentExpirience;
}

double Character::maxWeight() const
{
  return mMaxWeight;
}

void Character::changeHealth(qint32 value)
{
  mCurrentHealth += value;
  emit healthChanged(mCurrentHealth, mMaxHealth);
}

void Character::changeWeight(double value)
{
  mCurrentWeight += value;
  emit weightChanged(mCurrentWeight, mMaxWeight);
}

void Character::changeExpirience(qint32 value)
{
  mCurrentExpirience += value;
  emit expirienceChanged(mCurrentExpirience);
}

void Character::changeStats(StatMap& value)
{
  mStats.change(value);
  emit statsChanged(mStats.getStats());
}

void Character::changeSkills(SkillMap& value)
{
  mSkills.change(value);
  emit skillsChanged(mSkills.getSkills());
}

void Character::changeLevel()
{
  qint32 xp_cap = QConstants::START_XP + QConstants::ADDITIONAL_XP_PER_LEVEL * mCurrentLevel;
  if(mCurrentExpirience > xp_cap)
    {
      mCurrentExpirience -= xp_cap;
      ++mCurrentLevel;
      emit expirienceChanged(mCurrentExpirience);
      emit levelChanged(mCurrentLevel);
    }
}
