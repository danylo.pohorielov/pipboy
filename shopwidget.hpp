#ifndef SHOPWIDGET_H
#define SHOPWIDGET_H

#include <QDialog>
#include <QObject>
#include <QWindow>
#include <QGuiApplication>
#include <QMainWindow>

#include "customentitymodel.hpp"
#include "Items/Widgets/inventoryitemswidget.hpp"

class ShopWidget : public QWidget
{
  Q_OBJECT
public:
  ShopWidget(CustomEntityModel* right,
             ListPictureWidget* base,
             QWidget* parent = nullptr);

  void onSelectedRight();

  void onSelectedLeft();

  void moveRight(const QModelIndex &index);

  void moveLeft(const QModelIndex &index);

  void updatePrice();

  void showItemSellerModel(const QModelIndex &index);

private:
  CustomEntityModel* mLeft;
  ListPictureWidget* mBase;
  QTableView* mRightView;
  QGridLayout* mLayout;
  QLabel* mPrice;
  QWidget* mMainWidget;
  QVBoxLayout* mMainLayout;
  qint32 mCurrentPrice = 0;
};

#endif // SHOPWIDGET_H
