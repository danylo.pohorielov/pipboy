#include "lpwwithstatsnskils.hpp"

LPWWithStatsNSkils::LPWWithStatsNSkils(DescriptionWidget* descriptionWidget,
                                       CustomEntityModel* model, QWidget* parent)
  : ListPictureWidget(descriptionWidget, model, parent)
{
  mStats  = new QLabel(this);
  mSkills = new QLabel(this);

  mLayout->addWidget(mStats, 2, 0);
  mLayout->addWidget(mSkills, 2, 1);

  mStats->hide();
  mSkills->hide();
}

void LPWWithStatsNSkils::showStats(StatMap& stats)
{

}

void LPWWithStatsNSkils::showSkills(SkillMap& skills)
{

}
