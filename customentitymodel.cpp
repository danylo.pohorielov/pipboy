#include "customentitymodel.hpp"

CustomEntityModel::CustomEntityModel(std::unique_ptr<EntityReader> reader, QObject *parent)
  : QAbstractListModel{parent}, mReader(std::move(reader))
{
  mEntities = std::make_unique<QVector<Entity*>>();
  assignEntities(mReader->readFile());
  countWeight();
}

CustomEntityModel::CustomEntityModel(std::unique_ptr<std::vector<std::unique_ptr<Entity> > > entities, QObject *parent)
{
  assignEntities(std::move(entities));
}

void CustomEntityModel::addEntity(Entity *entity)
{
  beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
  mEntities->append(entity);
  endInsertRows();
  mCurrentWeight += entity->weight();
}

void CustomEntityModel::removeEntity(Entity *entity)
{
  beginRemoveRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
  mEntities->removeOne(entity);//removeAt(rowIndex);
  endRemoveRows();
  mCurrentWeight -= entity->weight();
}

Entity *CustomEntityModel::getEntity(const QModelIndex &index)
{
  if (!index.isValid())
             return nullptr;

  if (index.row() >= mEntities->size())
             return nullptr;
  return mEntities->at(index.row());
}

Entity* CustomEntityModel::popEntity(const QModelIndex &index)
{
  if (!index.isValid())
             return nullptr;

  if (index.row() >= mEntities->size())
             return nullptr;
  Entity* entity{mEntities->at(index.row())};
  removeEntity(mEntities->at(index.row()));
  return entity;
}

void CustomEntityModel::assignEntities(std::unique_ptr<std::vector<std::unique_ptr<Entity> > > entities)
{
  mEntities->reserve(entities->size());
  for(auto& entity : *entities)
    {
      mEntities->append(static_cast<Entity*>(entity.release()));
    }
}

void CustomEntityModel::countWeight()
{
  for(auto& entity : *mEntities)
    {
      mCurrentWeight += entity->weight();
    }
}

double CustomEntityModel::currentWeight() const
{
  return mCurrentWeight;
}

int CustomEntityModel::rowCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);
  return mEntities->size();
}

int CustomEntityModel::columnCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);
  return 2;
}

QVariant CustomEntityModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
             return QVariant();

  if (index.row() >= mEntities->size())
             return QVariant();

  Entity* entity = mEntities->at(index.row());
  if(role == Qt::DisplayRole)
    {
      if(index.column() == 0)
        {
          if (entity->amount() > 1)
            {
              return '(' + QString::number(entity->amount()) + ')';
            }
        }
      else
        {
          return entity->name();
        }
    }
  return QVariant();
}

bool CustomEntityModel::insertRows(int row, int count, const QModelIndex &parent)
{
  Q_UNUSED(parent);
  beginInsertRows(QModelIndex(), row, row + count - 1);
  for (int i = 0; i < count; ++i) {
      mEntities->insert(row + i, nullptr); // Insert null pointers for now
  }
  endInsertRows();
  return true;
}

bool CustomEntityModel::removeRows(int row, int count, const QModelIndex &parent)
{
  Q_UNUSED(parent);
  beginRemoveRows(QModelIndex(), row, row + count - 1);
  for (int i = 0; i < count; ++i) {
      mEntities->removeAt(row);
  }
  endRemoveRows();
  return true;
}

Qt::ItemFlags CustomEntityModel::flags(const QModelIndex &index) const
{
  if (!index.isValid() || index.row() >= mEntities->size())
            return Qt::NoItemFlags;

  if(index.column() == 1)
    {
      return QAbstractItemModel::flags(index);
    }
  return Qt::NoItemFlags;
}
