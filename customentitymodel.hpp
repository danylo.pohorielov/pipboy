#ifndef CustomEntityModel_HPP
#define CustomEntityModel_HPP

#include <QAbstractListModel>
#include <QObject>

#include "Items/Back/item.hpp"

class CustomEntityModel : public QAbstractListModel
{
  Q_OBJECT
public:
  CustomEntityModel(std::unique_ptr<EntityReader> reader, QObject *parent = nullptr);

  CustomEntityModel(std::unique_ptr<std::vector<std::unique_ptr<Entity> > > items, QObject *parent = nullptr);

  void addEntity(Entity* item);

  void removeEntity(Entity* item);

  Entity* getEntity(const QModelIndex &index);

  Entity* popEntity(const QModelIndex &index);

  void assignEntities(std::unique_ptr<std::vector<std::unique_ptr<Entity> > > items);

  void countWeight();

private:
  std::unique_ptr<QVector<Entity*>> mEntities;
  std::unique_ptr<EntityReader> mReader;
  double mCurrentWeight = 0;

  // QAbstractItemModel interface
public:
  int rowCount(const QModelIndex &parent) const override;
  int columnCount(const QModelIndex &parent) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  bool insertRows(int row, int count, const QModelIndex &parent) override;
  bool removeRows(int row, int count, const QModelIndex &parent) override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  double currentWeight() const;
};

#endif // CustomEntityModel_HPP
