#include "shopwidget.hpp"
#include <QHeaderView>

ShopWidget::ShopWidget(CustomEntityModel* right,
                       ListPictureWidget* base,
                       QWidget* parent)
  : QWidget{parent}, mLeft{right}, mBase{base}
{
  mBase->setParent(this);
  //mMainWidget     = new QWidget(this);
  mMainLayout     = new QVBoxLayout(this);
  //mLayout         = new QGridLayout(mMainWidget);
  mRightView      = new QTableView(this);
  mPrice          = new QLabel(this);

  setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

  // Add mMainWidget to the layout of ShopWidget
  //QVBoxLayout* shopLayout = new QVBoxLayout(this);
  //shopLayout->addWidget(mMainWidget);
  //mMainLayout->addWidget(mMainWidget);
  setLayout(mMainLayout);
  //mMainWidget->setLayout(mLayout);

  mMainLayout->addWidget(mBase);

  mRightView->setModel(right);
  mRightView->setMinimumSize(mBase->view()->size());
  mBase->layout()->addWidget(mRightView, 0, 2);
  mBase->layout()->addWidget(mPrice, 1, 2);

  mPrice->setMaximumHeight(100);
  connect(mBase->view(), &QTableView::clicked,
          this, &ShopWidget::moveRight);
  connect(mRightView, &QTableView::clicked,
          this, &ShopWidget::moveLeft);
  connect(mRightView, &QTableView::entered,
          this, &ShopWidget::showItemSellerModel);

  mRightView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  mRightView->setEditTriggers(QAbstractItemView::NoEditTriggers);

  mRightView->verticalHeader()->setVisible(false);
  mRightView->horizontalHeader()->setVisible(false);

  mRightView->setMouseTracking(true);
  mRightView->setStyleSheet("QTableView::item:selected { border: 2px solid green; }"
                           "QTableView::item:hover { background-color: rgba(0, 255, 0, 100); }");

  setMinimumSize(1000, 600); // Set minimum size instead of fixed width and height
  updatePrice();
}


void ShopWidget::moveRight(const QModelIndex &index)
{
  Entity* entity = mBase->model()->popEntity(index);
  if(entity != nullptr)
    {
      mLeft->addEntity(entity);
      mCurrentPrice += entity->price();
    }
  updatePrice();
  mBase->view()->reset();
}

void ShopWidget::moveLeft(const QModelIndex &index)
{
  Entity* entity = mLeft->popEntity(index);
  if(entity != nullptr)
    {
      mBase->model()->addEntity(entity);
      mCurrentPrice -= entity->price();
    }
  updatePrice();
  mRightView->reset();
}

void ShopWidget::updatePrice()
{
  mPrice->setText(QString::number(mCurrentPrice));
}

// optimize to use 1 method
void ShopWidget::showItemSellerModel(const QModelIndex &index)
{
  Entity* entity = mLeft->getEntity(index);
  if(entity != nullptr)
    {
      mBase->showImage(entity->name());
      mBase->fillInfo(entity);
    }
}
