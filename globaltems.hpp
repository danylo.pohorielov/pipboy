#ifndef GLOBALTEMS_HPP
#define GLOBALTEMS_HPP

#include <QString>

#include "Items/Back/weapon.hpp"
#include "Items/Back/apparel.hpp"
#include "Items/Back/aid.hpp"
#include "Items/Back/misc.hpp"
#include "Items/Back/ammo.hpp"
#include "customentitymodel.hpp"

class Globaltems
{
public:
  Globaltems();

  CustomEntityModel* getItems(QString name);

private:
  const std::map<QString, CustomEntityModel*> mItems =
    {
      {"Weapons", new CustomEntityModel{
         std::make_unique<WeaponReader>(":/all_items/resources/all_items/weapons.txt")}},
      {"Apparel", new CustomEntityModel{
         std::make_unique<ApparelReader>(":/all_items/resources/all_items/apparel.txt")}},
      {"Aid",     new CustomEntityModel{
         std::make_unique<AidReader>(":/all_items/resources/all_items/aid.txt")}},
      {"Misc",    new CustomEntityModel{
         std::make_unique<MiscReader>(":/all_items/resources/all_items/misc.txt")}},
      {"Ammo",    new CustomEntityModel{
         std::make_unique<AmmoReader>(":/all_items/resources/all_items/ammo.txt")}}
    };
};

#endif // GLOBALTEMS_HPP
