#ifndef LISTPICTUREWIDGET_H
#define LISTPICTUREWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QTableView>
#include <QStringListModel>

#include "customentitymodel.hpp"
#include "descriptionwidget.hpp"

class ListPictureWidget : public QWidget
{
  Q_OBJECT
public:
  explicit ListPictureWidget(DescriptionWidget* descriptionWidget, CustomEntityModel* model, QWidget *parent = nullptr);

  void showImage(QString name);

  QTableView *view() const;

  QGridLayout *layout() const;

  CustomEntityModel *model() const;

  void fillInfo(Entity* entity);

public slots:

  void showInfo(const QModelIndex& index);

protected:
  QGridLayout* mLayout;
  QLabel* mImage;
  QTableView* mView;
  QString mPath = ":/icons/resources/icons/";
  CustomEntityModel* mModel;
  DescriptionWidget* mDescription;
};

#endif // LISTPICTUREWIDGET_H
